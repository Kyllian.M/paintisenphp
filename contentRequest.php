<?php

session_start();
if ($_SESSION['user_id'] != 0) {

    function chargerClasse($classe) {
        require  $classe . '.php';
    }

    spl_autoload_register('chargerClasse');
    chargerClasse('BddManager');
    chargerClasse('User');
    $dataBase = new BddManager('localhost', 'chris', '53132834353', "Paint'ISEN");
    $user = new User($dataBase->buildUser($_SESSION['user_id']));
    $_SESSION['profil_link'] = $user->getUsername();
    $profil = new User($dataBase->buildUser("", $_SESSION['profil_link']));
}

if(isset($_POST["offset"])){
    $offset = $_POST["offset"];
}
else{
    $offset = null;
}

if(isset($_POST["imgSearch"])){
    $imgSearch = $_POST["imgSearch"];
}
else{
    $imgSearch = null;
}

if(isset($_POST["profilId"])){
    $profilId = $_POST["profilId"];
}
else{
    $profilId = $user->getUser_id();
}

if(isset($_POST["permission"])){
    $permission = $_POST["permission"];
}
else{
    $permission = null;
}

if(isset($_POST["userId"])){
    $userId = $_POST["userId"];
}
else{
    $userId = null;
}

echo $dataBase->displayPosts($profilId,$imgSearch,$permission,$userId,$offset);