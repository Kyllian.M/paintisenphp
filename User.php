<?php
    class User{
        private $username; //Nom d'utilisateur
        private $user_id; //ID de l'utilisateur
        private $profil_image; //URL de l'image de profil de l'utilisateur
        private $user_permissions; //Niveau de permission de l'utilisateur
        
        function __construct($data){ //Construction de l'objet utilisateur grâce à la méthode buildUser de l'objet BddManager (se référer à BddManager.php)
            $this->username = $data['username'];
            $this->user_id = $data['user_id'];
            $this->profil_image = $data['profil_image'];
            $this->user_permissions = $data['user_permissions'];
        }

        public function getUsername(){ //Accesseur renvoyant le nom d'utilisateur de l'objet Utilisateur
            return $this->username;
        }
        public function getUser_id(){ //Accesseur renvoyant le l'ID de l'objet Utilisateur
            return $this->user_id;
        }
        public function getProfil_image(){ //Accesseur renvoyant l'URL de l'image de profil de l'objet Utilisateur
            return $this->profil_image;
        }
        public function getUser_permissions(){ //Accesseur renvoyant le niveau de permissoin de l'objet Utilisateur
            return $this->user_permissions;
        }
        public function setUsername($name){ //Mutateur modifiant le nom d'utilisateur de l'objet Utilisateur
            $this->username = $name;
        }
        public function setProfil_image($url){ //Mutateur modifiant le nom d'utilisateur de l'objet Utilisateur
            $this->profil_image = $url;
        }
        public function setUser_permissions($permission){ //Mutateur modifiant le nom d'utilisateur de l'objet Utilisateur
            $this->user_permissions = $permission;
        }
    }
?>
