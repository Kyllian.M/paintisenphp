<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>paint</title>
        <meta name="description" content="">
        <meta name="author" content="satur">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="/favicon.ico">
        <link rel="apple-touch-icon" href="/apple-touch-icon.png">
        <link id="cssLINK" rel="stylesheet" href="css.css" />
    </head>
    <header>
        <div class="headerTools" id="TB-back">
            <
        </div>
        <div class="headerTools" id="TB-fichier" onclick="Qid('inputFile').style.display = 'block';">
            Fichier
            <input id="inputFile" type="file" style="display: none;"/>
        </div>
        <div class="headerTools" id="TB-info" onclick="if(Qid('desc-area').style.display === 'none'){Qid('desc-area').style.display = 'block';}else{Qid('desc-area').style.display = 'none';}">
            Info
        </div>
        <div class="headerTools" id="TB-affichage">
            Affichage
        </div>
        <div class="headerTools" id="TB-outils">
            Outils
        </div>
        <div class="headerTools" id="TB-quitter" onclick="">
            <a href="../profil.php" id="save-image" >Poster</a>
        </div>
    </header>
    <body>
        <input name="name" value="Mon Super Titre" id="name"/>
        <div id="toolbar-left">
            <div class="tool-left" id="tool-brush" onclick="circle()">
                <img src="toolbar/brush.png" alt="Voici un vrai navigateur https://www.google.com/chrome/"/>
            </div>
            <div class="tool-left" id="tool-eraser" onclick="eraser()">
                <img src="toolbar/eraser.png" alt="Voici un vrai navigateur https://www.google.com/chrome/"/>
            </div>
            <div class="tool-left" id="tool-cursor" onclick="pencil()">
                <img src="toolbar/cursor.png" alt="Voici un vrai navigateur https://www.google.com/chrome/"/>
            </div>
            <div class="tool-left" id="tool-smile" onclick="smile()">
                <img src="toolbar/smile.png" alt="Voici un vrai navigateur https://www.google.com/chrome/"/>
            </div>
        </div>
        <img src="bubble-left.png" id="bubble-left" alt="Voici un vrai navigateur https://www.google.com/chrome/"/>
        <img src="bubble-right.png" id="bubble-right" alt="Voici un vrai navigateur https://www.google.com/chrome/"/>
        <div id="color-picked"></div>
        <div id="color-picked-r">r = 0</div>
        <div id="color-picked-g">g = 0</div>
        <div id="color-picked-b">b = 0</div>
        <div id="toolbar-right">
            <div class="tool-right" id="tool-bucket" onclick="fill();">
                <img src="toolbar/bucket.png" alt="Voici un vrai navigateur https://www.google.com/chrome/"/>
            </div>
            <div class="tool-right" id="tool-picker" onclick="">
                <img src="toolbar/picker.png" alt="Voici un vrai navigateur https://www.google.com/chrome/"/>
            </div>
            <div class="tool-right" id="tool-text" onclick="textMode()">
                <img src="toolbar/text.png" alt="Voici un vrai navigateur https://www.google.com/chrome/"/>
            </div>
            <div class="tool-right" id="tool-colors" onclick="">
                <img src="toolbar/colors.png" alt="Voici un vrai navigateur https://www.google.com/chrome/"/>
            </div>
        </div>
        <canvas  id="canvas" style="z-index: 100; display: 'none'" onmousemove="paint(event);" onmousedown="activate();" onmouseup="deactivate();" onmouseout="deactivate()"></canvas>
        <canvas  id="canvas-back" style="z-index: 1; display: 'none'"></canvas>
        <canvas  id="canvas-grad" onmouseup="isPicking = false" onmousedown="isPicking = true" onmousemove="pickColor(event)" onmouseout="isPicking = false"></canvas>
        <div id="canvas-holder">
            <textarea id="text-area" onkeypress="printText(event);" spellcheck="false" onmousedown="movetext = true;" onmouseout="movetext = false" onmouseup="movetext = false" onmousemove="moveToY(event)" style="
                      position: absolute;
                      left: 353px;
                      top: 173px;
                      z-index: 101;
                      width: calc(100% - 700px);
                      height: 60px;
                      background-color: rgba(0,0,0,0.5);
                      color: rgba(255,255,255,1);
                      font-size: 45px;
                      text-align: center;
                      border: none;
                      resize: none;
                      font-family: monospace;
                      display: none;
                      ">Ton texte ici 😁</textarea>
        </div>
        <a href="#" onclick="pencil();">
            <img src="pencil.png" alt="Voici un vrai navigateur https://www.google.com/chrome/"/>
        </a>
        <a href="#" onclick="eraser();">
            <img src="eraser.png" alt="Voici un vrai navigateur https://www.google.com/chrome/"/>
        </a>
        <a href="#" onclick="fill();">
            <img src="bucket.png" alt="Voici un vrai navigateur https://www.google.com/chrome/"/>
        </a>
        <a href="#" onclick="square();">
            <img src="square.png" alt="Voici un vrai navigateur https://www.google.com/chrome/"/>
        </a>
        <a href="#" onclick="circle();">
            <img src="circle.png" alt="Voici un vrai navigateur https://www.google.com/chrome/"/>
        </a>
        <div style="width: 10px; height: 10px;" onclick="setThickness(10);"></div><br />
        <div style="width: 20px; height: 20px;" onclick="setThickness(20);"></div><br />
        <div style="width: 30px; height: 30px;" onclick="setThickness(30);"></div><br />
        <div style="width: 40px; height: 40px;" onclick="setThickness(40);"></div><br />
        <div class="slidecontainer">
            <input type="range" min="0" max="255" value="255" class="slider" id="myRange">
        </div>
        <div class="slidecontainer">
            <input type="range" min="0" max="255" value="127" class="slider" id="myRange2" onmousemove="setDarkness()">
        </div>
        <div>
            <div id="pointExemple" style="position: fixed;left: 223px;top: 50px;font-size: 60px;font-family: Calibri;">●</div>
            <div id="thicknessPlus" style="position: fixed;left: 240px;top: 110px;font-size: 50px" onclick="thickness++; Qid('thickness').innerHTML = thickness;">+</div>
            <div id="thicknessPlusDix" style="position: fixed;left: 212px;top: 110px;font-size: 50px" onclick="thickness+=10; Qid('thickness').innerHTML = thickness;">+</div>
            <div id="thickness" style="position: fixed;left: 215px;top: 151px;font-size: 50px">5</div>
            <div id="thicknessMois" style="position: fixed;left: 240px;top: 190px;font-size: 50px" onclick="if(thickness > 1){thickness--;} Qid('thickness').innerHTML = thickness;">-</div>
            <div id="thicknessMoisDix" style="position: fixed;left: 212px;top: 190px;font-size: 50px" onclick="if(thickness > 0){thickness-=10;} Qid('thickness').innerHTML = thickness;">-</div>
        </div>
        <div>
            <div id="brush-circle" style="position: fixed;left: 165px;top: 240px;font-size: 100px;font-family: Calibri;" onclick="circle()">●</div>
            <div id="brush-square" style="position: fixed;left: 215px;top: 238px;font-size: 95px;font-family: Calibri;" onclick="drawmode='square'">■</div>
            <div id="brush-triangle" style="position: fixed;left: 260px;top: 260px;font-size: 70px;font-family: Calibri;" onclick="drawmode='triangle'">▲</div>
        </div>
        <div>
            <div id="pos-x" style="position: fixed;left: 215px;top: 370px;font-size: 35px;font-family: Calibri;">x = 0</div>
            <div id="pos-y" style="position: fixed;left: 215px;top: 410px;font-size: 35px;font-family: Calibri;">y = 0</div>
        </div>
        <div style="position: fixed;left: 176px;top: 492px;font-size: 23px;font-family: Calibri;width: 155px">
            <?php
            $str = "😀,😁,😂,🤣,😃,😄,😅,😆,😉,😡,🤯,🤞,✌,️,🤟,🤘,👌,🖕,🐳,🐋,🦈,🐊,🐅,🌭,🍔,🍟,🍕,💯,😊,😋,😎,😍,😘,😗,😙,😚,️,🙂,🤗,🤩,🤔,🤨,😐,😑,😶,🤬,😷,🤒,🤕,🤢,🤮,🤧,😇,⚽,🏀,🏈,⚾,🍺,🍻,🥂,🍷,👍,👎,👊,✊,🤛,🤜,🤞,💵";
            $tab = explode(',', $str);
                for($i = 0;$i<count($tab);$i++){
                    echo '<button style="padding: 0;border: none;background: none;" onclick=\'emoji = this.innerHTML;actualiseExemple()\' >'. $tab[$i].'</button>';
                }
            ?>
        </div>
        
        <textarea id="desc-area">Ma Super Description</textarea>

        <script id="jsLINK" src="js.js"></script>
        <script>
            var css = document.querySelector("#cssLINK");
            css.href = "" + css.href + "?ChromeCacheDeMerde" + Math.random() * 1000;
            var js = document.querySelector("#jsLINK");
            js.src = "" + js.src+ "?ChromeCacheDeMerde" + Math.random() * 1000;
        </script>
        <script>
            document.getElementById("save-image").addEventListener("click", sendImageToPHP, false);
        </script>
    </body>
</html>