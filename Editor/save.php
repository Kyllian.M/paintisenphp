<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        session_start();
        if ($_SESSION['user_id'] != 0) {
            function chargerClasse($classe) {
                require  "../". $classe . '.php';
            }
            spl_autoload_register('chargerClasse');
            chargerClasse('BddManager');
            chargerClasse('User');
            $dataBase = new BddManager('localhost', 'chris', '53132834353', "Paint'ISEN");
            $user = new User($dataBase->buildUser($_SESSION['user_id']));
            $_SESSION['profil_link'] = $user->getUsername();
            $profil = new User($dataBase->buildUser("", $_SESSION['profil_link']));
        }

        $img = $_POST['imgData'];
        $img = str_replace("data:image/png;base64,", "", $img);
        $img = str_replace(" ", "+", $img);
        $fileData = base64_decode($img);
        $nomOeuvre = $_POST['imgname'];
        $descOeuvre = $_POST['imgdesc'];
        
        date_default_timezone_set('Europe/Paris');
        $date = date('m/d/Y h:i:s a', time());
        
        $fileName = str_replace(" ","-",$user->getUsername() ."-". $nomOeuvre . "-" . $user->getUser_id() ."-". rand(0, 10000) . ".png");
        file_put_contents($fileName, $fileData);
        $dataBase->createPost("Editor/".$fileName,$nomOeuvre,$descOeuvre, $user->getUser_id(), "0"); 

        echo "HAHA YES <br> <img src='" . $fileName . "'/>";
        ?>
    </body>
</html>
