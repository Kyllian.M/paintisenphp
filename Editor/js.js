var color = "#000000";
var thickness = 5;
var isPainting = false;
var x = null;
var y = null;
var xold, yold;
var LF, HF;
var drawmode = "pencil";
var canvasBack;
var canvasFront;
var opacity = 255;
var slider = document.getElementById("myRange");
var canvasX = 353;
var canvasY = 173;
var emoji = "😀";
var movetext = false;
var isPicking = false;


document.getElementById("canvas").style.left = canvasX + "px";
document.getElementById("canvas").style.top = canvasY + "px";
document.getElementById("canvas-back").style.left = canvasX + "px";
document.getElementById("canvas-back").style.top = canvasY + "px";
document.getElementById("canvas").style.display = "none";
document.getElementById("canvas-back").style.display = "none";

document.addEventListener("DOMContentLoaded", main(event));

function main(event) {
    Qid("inputFile").onchange = function () {
        slider.oninput = function () {
            opacity = this.value;
        };
        canvasBack = Qid("canvas-back");
        canvasFront = Qid("canvas");
        var img = getSelectedImage();
        img.onload = function () {
            canvasFront.style.display = "block";
            canvasBack.style.display = "block";
            var destCtx = canvasBack.getContext('2d');
            var canvasImg = convertImageToCanvas(img);
            LF = getWidth() - 700;
            HF = LF * (canvasImg.height / canvasImg.width);
            canvasFront.width = LF;
            canvasBack.width = LF;
            canvasFront.height = HF;
            canvasBack.height = HF;
            destCtx.drawImage(canvasImg, 0, 0, LF, HF);
        };

        this.style.display = 'none';
    };
    var url2 = "gradient.png";
    var img2 = new Image;
    var canvasgrad = Qid("canvas-grad");
    img2.onload = function () {
        var destCtx = canvasgrad.getContext('2d');
        var canvasImg = convertImageToCanvas(img2);
        canvasgrad.width = canvasImg.width;
        canvasgrad.height = canvasImg.height;
        destCtx.drawImage(canvasImg, 0, 0);
    };
    img2.src = url2;
}


function paint(event) {
    var canvas = document.getElementById("canvas");
    var ctx = canvas.getContext("2d");
    xold = x;
    yold = y;
    var bodyRect = document.body.getBoundingClientRect();
    var elemRect = canvas.getBoundingClientRect();
    var offsetX = elemRect.left - bodyRect.left;
    var offsetY = elemRect.top - bodyRect.top;
    x = event.clientX - offsetX;
    y = event.clientY - offsetY + window.scrollY;
    if (isPainting) {
        var r = parseInt(color.substr(1, 2), 16);
        var g = parseInt(color.substr(3, 2), 16);
        var b = parseInt(color.substr(5, 2), 16);
        var a = opacity;
        ctx.strokeStyle = color;
        ctx.moveTo(xold, yold);
        ctx.lineWidth = thickness;
        ctx.stroke();
        switch (drawmode) {
            case "pencil":
                traceLigne(ctx, xold, yold, x, y, r, g, b, a);
                break;
            case "eraser":
                traceLigneShape(ctx, xold, yold, x, y, 1, 0, 0, 0, function (ctx2, xi, yi, ri, gi, bi, ai) {
                    traceCarre(ctx2, xi, yi, thickness, ri, gi, bi, ai);
                });
                break;
            case "square" :
                ctx.fillStyle = "rgba(" + r + "," + g + "," + b + ',' + a / 255 / 2.5 + ')';
                ctx.font = 'italic ' + thickness + 'pt Calibri';
                traceLigneShape(ctx, xold, yold, x, y, 1, 0, 0, 0, function (ctx2, xi, yi, ri, gi, bi, ai) {
                    ctx.fillText("■", xi, yi);
                });
                break;
            case "fill" :
                autoFill(ctx, x, y, r, g, b, a);
                break;
            case "circle" :
                ctx.fillStyle = "rgba(" + r + "," + g + "," + b + ',' + a / 255 / 2.5 + ')';
                ctx.font = 'italic ' + thickness + 'pt Calibri';
                traceLigneShape(ctx, xold, yold, x, y, 1, 0, 0, 0, function (ctx2, xi, yi, ri, gi, bi, ai) {
                    ctx.fillText("●", xi, yi);
                });
                break;
            case "smile" :
                ctx.font = thickness + 'pt Calibri';
                ctx.fillStyle = "rgba(" + r + "," + g + "," + b + ',' + a / 255 + ')';
                var random = Math.floor(Math.random() * 5);
                var char = emoji;
                ctx.fillText(char, x, y);
                break;
            case "triangle":
                ctx.fillStyle = "rgba(" + r + "," + g + "," + b + ',' + a / 255 / 2.5 + ')';
                ctx.font = 'italic ' + thickness + 'pt Calibri';
                traceLigneShape(ctx, xold, yold, x, y, 1, 0, 0, 0, function (ctx2, xi, yi, ri, gi, bi, ai) {
                    ctx.fillText("▲", xi, yi);
                });
            break;
            default :
                drawmode = pencil;
                break;
        }
    }
}

function setDarkness(){
    var darkness  = Qid("myRange2").value;
    var r = parseInt(colorstatic.substr(1, 2), 16);
    var g = parseInt(colorstatic.substr(3, 2), 16);
    var b = parseInt(colorstatic.substr(5, 2), 16);
    r = Math.floor(r + (darkness - 127)*2);
    g = Math.floor(g + (darkness - 127)*2);
    b = Math.floor(b + (darkness - 127)*2);
    if(r > 255){
        r = 255;
    }
    if(g > 255){
        g = 255;
    }
    if(b > 255){
        b = 255;
    }
    if(r < 0){
        r = 0;
    }
    if(g < 0){
        g = 0;
    }
    if(b < 0){
        b = 0;
    }
    var a = opacity;
    color = "#" + r.toString(16) + g.toString(16) + b.toString(16) + a.toString(16);
        Qid("color-picked").style = "background-color: " + color + ";";
        actualiseExemple();
        Qid("color-picked-r").innerHTML = "r = " + r;
        Qid("color-picked-g").innerHTML = "g = " + g;
        Qid("color-picked-b").innerHTML = "b = " + b;
}

function actualiseExemple() {
    var ex = Qid("pointExemple");
    switch (drawmode) {
        case "circle" :
            ex.innerHTML = "●";
            break;
        case 'square' :
            ex.innerHTML = "■";
            break;
        case "smile" :
            ex.innerHTML = emoji;
            break;
    }
    ex.style.color = color;
}

function moveToY(event) {
    var txt = Qid("text-area");
    if (movetext) {
        var height = event.clientY - parseInt(txt.style.height.replace("px", "")) / 2;
        if (height < canvasFront.clientHeight + canvasY - txt.clientHeight && height > canvasY) {
            txt.style.top = height + "px";
        }
    }
}

function convertTextareaToCanvas(txtarea) {
    var canvas = document.createElement("canvas");
    canvas.width = txtarea.clientWidth + 10;
    canvas.height = txtarea.clientHeight;
    var ctx = canvas.getContext("2d");
    var rgba = txtarea.style.backgroundColor.replace("rgba(", "").replace(")").split(',');
    var r = parseInt(rgba[0]);
    var g = parseInt(rgba[1]);
    var b = parseInt(rgba[2]);
    var a = parseFloat(rgba[3]) * 255;
    for (var i = 0; i < canvas.width; i++) {
        for (var i2 = 0; i2 < canvas.height; i2++) {
            setPixelValueAt(ctx, i, i2, r, g, b, a);
        }
    }
    var str = txtarea.value;
    ctx.fillStyle = txtarea.style.color;
    ctx.font = (txtarea.style.fontSize.replace("px", "") - 10) + 'pt ' + txtarea.style.fontFamily;
    ctx.fillText(str, canvas.width / 2 - ctx.measureText(str).width / 2, 45);
    canvasFront.getContext('2d').drawImage(canvas, 0, parseInt(txtarea.style.top.replace("px", "")) - canvasY);
}

var colorstatic;

function pickColor(event) {
    if (isPicking) {
        var ctx = Qid("canvas-grad").getContext("2d");
        var r, g, b, a;
        var bodyRect = document.body.getBoundingClientRect(),
                elemRect = Qid("canvas-grad").getBoundingClientRect(),
                offsetX = elemRect.left - bodyRect.left,
                offsetY = elemRect.top - bodyRect.top;
        r = getPixelValueAt(ctx, event.clientX - offsetX, event.clientY - offsetY, 'r');
        g = getPixelValueAt(ctx, event.clientX - offsetX, event.clientY - offsetY, 'g');
        b = getPixelValueAt(ctx, event.clientX - offsetX, event.clientY - offsetY, 'b');
        a = getPixelValueAt(ctx, event.clientX - offsetX, event.clientY - offsetY, 'a');
        color = "#" + r.toString(16) + g.toString(16) + b.toString(16) + a.toString(16);
        colorstatic = color;
        Qid("color-picked").style = "background-color: " + color + ";";
        actualiseExemple();
        Qid("color-picked-r").innerHTML = "r = " + r;
        Qid("color-picked-g").innerHTML = "g = " + g;
        Qid("color-picked-b").innerHTML = "b = " + b;
    }
}

function getSelectedImage() {
    var file = Qid("inputFile").files[0];
    var imageType = /^image\//;

    if (imageType.test(file.type)) {
        var img = document.createElement("img");
        img.classList.add("obj");
        img.file = file;
        var reader = new FileReader();
        reader.onload = (function (aImg) {
            return function (e) {
                aImg.src = e.target.result;
            };
        })(img);
        reader.readAsDataURL(file);
        img.width = "1230";
    }
    return img;
}

function getWidth() {
    return Math.max(
            document.body.scrollWidth,
            document.documentElement.scrollWidth,
            document.body.offsetWidth,
            document.documentElement.offsetWidth,
            document.documentElement.clientWidth
            );
}

function getHeight() {
    return Math.max(
            document.body.scrollHeight,
            document.documentElement.scrollHeight,
            document.body.offsetHeight,
            document.documentElement.offsetHeight,
            document.documentElement.clientHeight
            );
}

function convertImageToCanvas(image) {
    var canvas = document.createElement("canvas");
    canvas.width = image.naturalWidth;
    canvas.height = image.naturalHeight;
    canvas.getContext("2d").drawImage(image, 0, 0);
    return canvas;
}

function activate() {
    isPainting = true;
}

function deactivate() {
    isPainting = false;
}

function removeTextArea() {
    Qid("text-area").style.display = "none";
}

function smile() {
    drawmode = 'smile';
    document.getElementById("canvas").style.cursor = "url('smile.png'), default";
    actualiseExemple();
}

function textMode() {
    drawmode = "text";
    document.getElementById("canvas").style.cursor = "url('text.png'), default";
    Qid("text-area").style.display = "block";
}

function eraser() {
    removeTextArea();
    document.getElementById("canvas").style.cursor = "url('eraser.png'), default";
    drawmode = "eraser";
}

function fill() {
    removeTextArea();
    document.getElementById("canvas").style.cursor = "url('bucket.png'), default";
    drawmode = "fill";
}

function square() {
    removeTextArea();
    document.getElementById("canvas").style.cursor = "url('square.png'), default";
    drawmode = "square";
    actualiseExemple();
}

function square() {
    removeTextArea();
    //document.getElementById("canvas").style.cursor = "url('square.png'), default";
    drawmode = "triangle";
    actualiseExemple();
}

function circle() {
    removeTextArea();
    document.getElementById("canvas").style.cursor = "url('circle.png'), default";
    drawmode = "circle";
    actualiseExemple();
}

function pencil() {
    removeTextArea();
    document.getElementById("canvas").style.cursor = "url('pencil.png'), default";
    color = document.getElementById("colors").value;
    drawmode = "pencil";
}

function setThickness(n) {
    thickness = n;
}

function save() {
    var canvas = document.createElement("canvas");
    canvas.width = canvasFront.width;
    canvas.height = canvasFront.height;
    canvas.getContext("2d").drawImage(canvasBack, 0, 0);
    canvas.getContext("2d").drawImage(canvasFront, 0, 0);
    var image = canvas.toDataURL("image/png");
    this.href = image;
}

function getXMLHttpRequest() {
    var xhr = null;
    if (window.XMLHttpRequest || window.ActiveXObject) {
        if (window.ActiveXOject) {
            try {
                xhr = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (error) {
                xhr = new ActiveXObject("Microsoft.XMLHTTP");
            }
        } else {
            xhr = new XMLHttpRequest();
        }
    } else {
        alert("Voici un vrai navigateur https://www.google.com/chrome/");
        return null;
    }
    return xhr;
}

function sendImageToPHP() {
    var canvas = document.createElement("canvas");
    canvas.width = canvasFront.width;
    canvas.height = canvasFront.height;
    canvas.getContext("2d").drawImage(canvasBack, 0, 0);
    canvas.getContext("2d").drawImage(canvasFront, 0, 0);
    var image = canvas.toDataURL("image/png");

    var ajax = getXMLHttpRequest();
    ajax.open("POST", "save.php", false);
    ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    ajax.onreadystatechange = function () {
        //console.log(ajax.responseText);
    };
    console.log(Qid("name").value);
    ajax.send("imgData=" + image + "&imgname=" + Qid("name").value + "&imgdesc=" + Qid("desc-area").value);
}

function getPixelValueAt(ctx, left, top, value) {
    var imageData = ctx.getImageData(left, top, 1, 1).data;
    switch (value) {
        case 'r':
            return imageData[0];
            break;
        case 'g':
            return imageData[1];
            break;
        case 'b':
            return imageData[2];
            break;
        case 'a':
            return imageData[3];
            break;
    }
}

function comparePixelData(ctx, left, top, r, g, b, a) {
    var ret = false;
    if (getPixelValueAt(ctx, left, top, 'r') === r && getPixelValueAt(ctx, left, top, 'g') === g && getPixelValueAt(ctx, left, top, 'b') === b && getPixelValueAt(ctx, left, top, 'a') === a) {
        ret = true;
    }
    return ret;
}

function setPixelValueAt(ctx, left, top, r, g, b, a) {
    var imageData = ctx.getImageData(left, top, 1, 1);
    imageData.data[0] = r;
    imageData.data[1] = g;
    imageData.data[2] = b;
    imageData.data[3] = a;
    ctx.putImageData(imageData, left, top);
}

function traceLigne(ctx, xa, ya, xb, yb, R, G, B, A) {
    var a;
    var c;
    var yi;
    var xi;
    function colorPixel() {
        if (!(xi > LF || yi > HF || xi < 0 || yi < 0)) {
            if (!comparePixelData(ctx, xi, yi, R, G, B, A)) {
                setPixelValueAt(ctx, xi, yi, R, G, B, A);
            }
        }
    }
    if (xb === xa) {
        a = 9999999;
    } else {
        a = (yb - ya) / (1. * xb - xa);
    }
    c = ya - a * xa;

    if (a >= -1 || a <= 1) {
        if (xa <= xb) {
            for (xi = xa; xi < xb; xi++) {
                yi = a * xi + c;
                colorPixel();
            }
        }
        if (xa > xb) {
            for (xi = xa; xi > xb; xi--) {
                yi = a * xi + c;
                colorPixel();
            }
        }
    }
    if ((a < -1. || a > 1.) && a !== 9999999) {
        if (ya <= yb) {
            for (yi = ya; yi < yb; yi++) {
                xi = (yi - c) * 1. / a;
                colorPixel();
            }
        }
        if (ya > yb) {
            for (yi = ya; yi > yb; yi--) {
                xi = (yi - c) * 1. / a;
                colorPixel();
            }
        }
    }
    if (a === 9999999) {
        if (ya <= yb) {
            for (yi = ya; yi < yb; yi++) {
                xi = xa;
                colorPixel();
            }
        }
        if (ya > yb) {
            for (yi = ya; yi > yb; yi--) {
                xi = xa;
                colorPixel();
            }
        }
    }
}

function traceLigneShape(ctx, xa, ya, xb, yb, R, G, B, A, func) {
    var a;
    var c;
    var yi;
    var xi;
    function colorPixel() {
        if (!(xi > LF || yi > HF || xi < 0 || yi < 0)) {
            func(ctx, xi, yi, R, G, B, A);
        }
    }
    if (xb === xa) {
        a = 9999999;
    } else {
        a = (yb - ya) / (1. * xb - xa);
    }
    c = ya - a * xa;

    if (a >= -1 || a <= 1) {
        if (xa <= xb) {
            for (xi = xa; xi < xb; xi++) {
                yi = a * xi + c;
                colorPixel();
            }
        }
        if (xa > xb) {
            for (xi = xa; xi > xb; xi--) {
                yi = a * xi + c;
                colorPixel();
            }
        }
    }
    if ((a < -1. || a > 1.) && a !== 9999999) {
        if (ya <= yb) {
            for (yi = ya; yi < yb; yi++) {
                xi = (yi - c) * 1. / a;
                colorPixel();
            }
        }
        if (ya > yb) {
            for (yi = ya; yi > yb; yi--) {
                xi = (yi - c) * 1. / a;
                colorPixel();
            }
        }
    }
    if (a === 9999999) {
        if (ya <= yb) {
            for (yi = ya; yi < yb; yi++) {
                xi = xa;
                colorPixel();
            }
        }
        if (ya > yb) {
            for (yi = ya; yi > yb; yi--) {
                xi = xa;
                colorPixel();
            }
        }
    }
}

function traceCarre(ctx, xa, ya, ep, r, g, b, a) {
    var xb = xa + ep;
    var yb = ya + ep;

    for (var i = xa; i < xb; i++) {
        setPixelValueAt(ctx, i, ya, r, g, b, a);
    }
    for (var i2 = ya; i2 < yb; i2++) {
        setPixelValueAt(ctx, xa, i2, r, g, b, a);
    }
    for (var i = xa; i < xb; i++) {
        setPixelValueAt(ctx, i, yb, r, g, b, a);
    }
    for (var i2 = ya; i2 < yb; i2++) {
        setPixelValueAt(ctx, xb, i2, r, g, b, a);
    }

    /*for (var i = xa; i < xb; i++) {
     for (var i2 = ya; i2 < yb; i2++) {
     setPixelValueAt(ctx, i, i2, r, g, b, a);
     }
     }*/

    /*traceLigne(ctx,xa,ya,xa,yb,r,g,b,a);
     traceLigne(ctx,xa,yb,xb,yb,r,g,b,a);
     traceLigne(ctx,xb,yb,xb,ya,r,g,b,a);
     traceLigne(ctx,xb,ya,xa,ya,r,g,b,a);
     autoFill(ctx,(xa+xb)/2,(ya+yb)/2,r,g,b,a);*/
}

function autoFill(ctx, x, y, r, g, b, a) { //Fonction de remplissage
    if ((x >= 0 && x <= LF) && (y >= 0 && y <= HF)) {
        setPixelValueAt(ctx, x, y, r, g, b, a);
        if (comparePixelData(ctx, x + 1, y, 0, 0, 0, 0) && (testFreeSpaceXp(ctx, x + 1, y) === true)) {
            autoFill(ctx, x + 1, y, r, g, b, a);
        }
        if (comparePixelData(ctx, x - 1, y, 0, 0, 0, 0) && (testFreeSpaceXn(ctx, x - 1, y) === true)) {
            autoFill(ctx, x - 1, y, r, g, b, a);
        }
        if (comparePixelData(ctx, x, y + 1, 0, 0, 0, 0) && (testFreeSpaceYp(ctx, x, y + 1) === true)) {
            autoFill(ctx, x, y + 1, r, g, b, a);
        }
        if (comparePixelData(ctx, x, y - 1, 0, 0, 0, 0) && (testFreeSpaceYn(ctx, x, y - 1) === true)) {
            autoFill(ctx, x, y - 1, r, g, b, a);
        }
    }
}

function testFreeSpaceXp(ctx, x, y) {
    var r = 0, g = 0, b = 0, a = 0;
    for (var i = x; i < LF; i++) {
        if (comparePixelData(ctx, x, y, r, g, b, a)) {
            return true;
        }
    }
    return false;
}

function testFreeSpaceYp(ctx, x, y) {
    var r = 0, g = 0, b = 0, a = 0;
    for (var i = y; i < LF; i++) {
        if (comparePixelData(ctx, x, y, r, g, b, a)) {
            return true;
        }
    }
    return false;
}

function testFreeSpaceXn(ctx, x, y) {
    var r = 0, g = 0, b = 0, a = 0;
    for (var i = x; i < LF; i--) {
        if (comparePixelData(ctx, x, y, r, g, b, a)) {
            return true;
        }
    }
    return false;
}

function testFreeSpaceYn(ctx, x, y) {
    var r = 0, g = 0, b = 0, a = 0;
    for (var i = y; i < LF; i--) {
        if (comparePixelData(ctx, x, y, r, g, b, a)) {
            return true;
        }
    }
    return false;
}

function printText(event) {
    if (event.key === "Enter") {
        event.preventDefault();
        convertTextareaToCanvas(Qid("text-area"));
        circle();
        Qid("text-area").style.display = "none";
    }
}

function traceCerclePixels(ctx, xT, yT, ep, r, g, b, a) {
    var canvas = document.createElement("canvas");
    canvas.width = 2 * ep + 2;
    canvas.height = 2 * ep + 2;
    var ctxCanvas = canvas.getContext("2d");
    var y, y2;
    var xp;
    var yp, yp2;
    var xpAnci = -1;
    var ypAnci = -1;
    var yp2Anci = -1;
    for (var i = -2; i < 2; i = i + 0.1) {
        y = Math.sqrt(4 - Math.pow(i, 2));
        y2 = -Math.sqrt(4 - Math.pow(i, 2));
        xp = ((ep + 1) - ep) + ((i + 2) * (((ep + 1) + ep) - ((ep + 1) - ep)) / (2 + 2));
        yp = ((ep + 1)) + ((y) * (((ep + 1) + ep) - ((ep + 1))) / (2));
        yp2 = ((ep + 1) - ep) + ((y2 + 2) * (((ep + 1)) - ((ep + 1) - ep)) / (2));
        if (xpAnci !== -1) {
            traceLigne(ctxCanvas, xpAnci, ypAnci, xp, yp, r, g, b, a);
            traceLigne(ctxCanvas, xpAnci, yp2Anci, xp, yp2, r, g, b, a);
        }
        xpAnci = xp;
        ypAnci = yp;
        yp2Anci = yp2;
    }
    xp = ((ep + 1) - ep) + ((2 + 2) * (((ep + 1) + ep) - ((ep + 1) - ep)) / (2 + 2));
    yp = ((ep + 1)) + ((0) * (((ep + 1) + ep) - ((ep + 1))) / (2));
    yp2 = ((ep + 1) - ep) + ((0 + 2) * (((ep + 1)) - ((ep + 1) - ep)) / (2));
    if (xpAnci !== -1) {
        traceLigne(ctxCanvas, xpAnci, ypAnci, xp, yp, r, g, b, a);
        traceLigne(ctxCanvas, xpAnci, yp2Anci, xp, yp2, r, g, b, a);
    }
    setPixelValueAt(ctxCanvas, ep + 1 + ep, ep + 1, r, g, b, a);
    if (ep > 1) {
        //autoFill(ctxCanvas, (ep + 1), (ep + 1), r, g, b, a);
        /*for(var i=0;i<canvas.width;i++){
         for(var i2;i2<canvas.height;i2++){
         if(testFreeSpaceXn(ctxCanvas,i,i2) && testFreeSpaceXp(ctxCanvas,i,i2)){
         setPixelValueAt(ctxCanvas,i,i2,r,g,b,a);
         }
         }
         }*/
    }
    for (var i = 0; i < (2 * ep + 2); i++) {
        for (var i2 = 0; i2 < (2 * ep + 2); i2++) {
            if (comparePixelData(ctxCanvas, i, i2, r, g, b, a) && (xT - ep + i > 0 && xT - ep + i < LF && yT - ep + i2 > 0 && yT - ep + i2 < HF)) {
                var tr = getPixelValueAt(ctxCanvas, i, i2, 'r');
                var tg = getPixelValueAt(ctxCanvas, i, i2, 'g');
                var tb = getPixelValueAt(ctxCanvas, i, i2, 'b');
                var ta = getPixelValueAt(ctxCanvas, i, i2, 'a');
                setPixelValueAt(ctx, xT - ep + i, yT - ep + i2, tr, tg, tb, ta);
            }
        }
    }
}



/*
 ````````````````````                                                                                                
 ``.-://+ooooooooosssssyssssoo+:-```                                                                                         
 ```.-:+osyhhhhhhhhhhhhhhhhhhhhhhhhhhhhhs+/-...`` ``                                                                                
 ``.-/oshhhhhhhhhyyhhhhhhhhhhhhhyyyyyyyyyyhhhhyyyys+:.`                                                                                
 `.:+yhhhhyhhhhhyyhhhhhhhhhhhhhhhhhhhhhyyyyhhhyyhhhhhhho-`                                                                               
 `-/shhhhhhyyyyhhhyhhhhhyyyyyyyyyhhhhhyhyyyyyhhhhyyyyyyhhhs-```                                                                            
 `-oyhhhyyyhhhhhhhhyyhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhs/:-.`                                                                          
 `:sdhhhyyyhhhhhhhhhhhhhyhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhy+-`                                                                        
 :shhyhyhyhhhhhhhhhhhhhhyhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhdhddh+-`                                                                      
 `.sdhhyyhyyhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhs-`                                                                     
 `:shhhyyyhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh+`                                                                     
 `:yhhyyhyhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhyhhhhhhhs-`    `                                                               
 `+hhyyhhyhhhhhhhhhhhhhhhhhhhhhhhyyyyyyhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhyo.``                                                                  
 .shhyhhhyhhhhhhhhhhhhhhhhhhhhhhhhhhyyhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhho:-``                                                                
 `-yhhyyyhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhs+:.`                                                              
 `:shhhhyyhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhs:`                                                             
 `  `-ohhyhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhdy:`                                                            
 `  `-ohhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhdo.                                                            
 `:shdhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhdhhhhhhhhhhhhhhhhdo.                                                            
 `/hddddddddddddhhhhhhyyssoo+++/////////////////////////////++o++++ooooooooossssyyyhhhhhhh+.                                                            
 `.sddddddddddddhhso++/:::---------::/:::---:::::::::::::---::///:::///::-:::::::::/+++oyhs.`                                                            
 `-sdddddddddddys++/::::-----------::::::::------------::::::::::::::::::---:::::::::::://:`                                                             
 `-+oosyhdddddyo+////:::-----------::-------::::::::::::--:::::::::::::::-:::::-------::-.`                                                              
 `----::/+oydds:-:////:---------------::::::::::::::::::::::::::::::::::::::::::::::::/:-`                                                               
 .:::::--::/+o+:--:///:::--------:---:::::::///////////////////:////////////////////::::.`                                                               
 ./++++/:::::/:---:::::::::-------------:::///++ooo++++++///////////++ooso+//////::::::-.`                                                               
 `://++ooo++//:--------:::-----------::::/+osyhhhhhhyosys+////::://ohddddhys////:::::::-.`                                                               
 .:/+++oooss+/---------:::----------::::+oshNddmNdhdyoyyo++//:///+hmmmNNdhho/+/:::::::--`                                                               
 .:+++oooo+//::::::--:::::--------::::::/+ydmmmmmmmysys+/////////+ydmmmmdyo++/:::::----.`                                                              
 ` ``://++o+/:://:-----:::::--------:::/:::://ossyysso++/////////////++osso++//::::----:-.`                                                              
 `.://+++/:://:----:::::::-----::::::::::::::::////////////////////////://///:::----::-`                                                              
 `-:///+/::///:--:://:::::---:::::::::::::::::::////////////////::::::::////::::----:-`                                                              
 `-::://///++:--::///::::::::::::::::::::::::::://///////////:::::::::::::::::::-----`                                                              
 `-:::://+oo/:--:://::::::::::::::::::::::---::://////////:::::::::::::::::::::-----`                                                              
 .::-://+++/::--:///::::::::::::::::::::::::::::////////::::::::::::::::::::::-----.`                                                             
 `-:::/+/:::::::::///::::::::::::::::::::::::::://////::::/::::::::::::::::::--::---`                                                             
 `-//+o/::::-::::::///::--:::::::::::::::::::://////:::///::::::::::::::::::-::::::.                                                             
 `.:/oo+:::::::::::::///::::::::::::::::://+++++++///////////////:::::::::----::--:-`                                                            
 `.-/sy+:::::::::-:::://///::::::::::::::::///++oooooo++++oooo++//::------.-----::-`                                                            
 `.:shmmo::::::::::::::::////::::::::::::::::::::///+++ooo+++++//:::------------:::-`                                                            
 `:ohmmNNdo:::::::::::::::::::::::::::::::::::::/::::::://////:::::---------------::/:`                                                            
 ` `.:smmNmNNNdo::::::::::::::::::--:::::::::///////////////////////++///:::::::::--:::::/:`                                                            
 `-+hmNmNNmmmNdo:::::::::::::::::::::::::////+++//++oosyyhhhhhyyyyssssoo+/::-------::::::/:`                                                            
 `-ohmNmmmmmmmmmmh+::::::::::::::::::::::://///////////+++oosssoooooooo++//::--------:::--://`                                                            
 `-ohNmmmNmmmmmmmmmms/:::::::-::::::::::::::::////////////+++++++++//++oo+//:---------:::::--/:`                                                            
 `  `.+hNNmmmNNmmmmmmmmNmy/:::::::::::::::::::::::::::/:///////+++++++++++++//::::::::::::----::-:/:`                                                            
 `:yNNNmNNNNNNmmNmmmmmmmy+--:::::::::::::::::::::::::::::///////////////:::://////:::::::--::--/+:`                                                            
 `-smNNNNmmNNNNNNNNmmmmmmNms/::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::-::--:::`                                                             
 `-odmNNNNNNNNNNNNNNNNmmmmmmmmdy+:::-::::-----------:::::::::::::::::::::::::::::::::::::::-------::-.`                                                            
 .odNNNNNNNNNNNNNNNNNNmmmmmmmmNNmdy/::::::-----------::::::::::::::::::::::::::::::::::---------------.`                                                            
 `.omNNNNNNNNNNNNNNNNNNNNmmmmmmmmmmNd+::::::------------:::::::::::::----------------------------:----.:+:.`      `                                                   
 ``.+hmNNNNNNNNNNNNNNNNNNNNNmmmmmmmmmNmmmy/:::------------------------------------------------------------omNds/--..--::/+//:--`                                         
 ` `:smmmmmNmNNNNNNNNNNNNNNNNNmmmmmmmmmmmmNmy/::----------------------------------------------------------:+yNNNNNmmmmmmmNNNNNmmdo.                                        
 `:hmdmddmmNNNNNNNNNNNNNNNNNNmmmmmmmmmmmmmmms--:---:-----------------------------------------------------:odNNNNNNNNmmmmmmmmmmNNd/`                                       
 `+dmddddmmmNNNNNNNNNNNNNNNNNNmmmmmmmmmmmmmNh+-------:-----------------------------------::-------------+hmNNNNNNNNNmmmmmmmmmmmNms.                                       
 `/hddmmmmmmmmmNNNNNNNNNNNNNNNNmmmmmmmmmmmmmmmds/:---:::------------------------------------------------:smNNNNNNNNNNmmmmmmmmmmmmmms-      ````..----:::-.```              
 .smdddddmdddmmmmNNNNNNNNNNNNNNNmmmmmmmmmmmmmmNmdho:---------------------------------------------------:+ymNNNNNNNNNNmmmmmddhyssoooo/---..--:::////////++o+/::--.```       
 `/hddmddddddmmmmmmNNNNNNNNNNNNNNmmmmmmmmmmmmmmmmmNd+----:--:-----------------------------------------:ohdmNNNNNNNNNNNmmmmddysoo++////////////////////////+ossooo+/:--`     
 -ohmdmddmmdmmmdmmmmmmNNNNNNNNNNNNmmmmmmmmmmmmmmmmmNmho/::::---:::-------------------------------------/hNNNNNNNNNNNNNNmmmmmhyoo+++////////////////////+oo++++++++oooo+/:.   
 `-sdmddmmmdddmmNmmmmmNmmmNNNNNNNNNmmmmmmmmmmmmmmmmmmmmmmddddhs+/:::--------------------------....------:+hNNNNNNNNNNNNNmmmmmmdso+///////////+/////+++/////++oossooo++++++/-`  
 `:ymmmmmmddhyyyhdmmmmmNNNNNNNNNNNNNmmmmmmmmmmmmmmmmmmmmmmmmmmNmmdyo+::////::----------------------:::::/symNNNNNNNNNNNNNNmmmmmdso+////////////+/////+osoo+/////++oooooo++//:.` 
 `:ymmmNNNdyyso++++oshdmmmNNNNNmmmNNNNNNmmmmmmmmmmmmmmmmmmmmmmmNmmmmmmmdddmdhso/:---------------::/oyddhydmmmmNNmNNNNNNNNNmmmmmmmho//////////////////::/+osysssooo++///+ooo++/:. 
 `:dmmmNmdhsoo+//////+ooosyhhhysoossyyhhhhyyyyhdmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmdhs+/////////////oyhdmmmmmmmmmmmNNNNNNNNNNNNmmmmmmmmhso++/://////////////:://++ooosssooo++////++/-`
 -yNNmmmdho++////////////+++++++++++++++++++//oydmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmdmmmmmmmmmmmmmmmmmmmmmmmNNNNNNNNNNNmmmmmmmmNNmmdhhs+://///////////////////++///+++++///:.`
 .sNNmmNmdo/////////////////////+++++//+/+++///+hmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmNNmmmmmmmmmmmmmmmmmNNNNNNNNNNNNmmNmmmNNNNNNmy/.`.-://///////////////////:::--::////-`
 .+mNmmmmms/:://////////////////+++ooooossoososydmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmNNNNNNNNNNNNNmmmmNNNNNNd:``  `.-//////:------:::::////:/:-``..-.`
 :dmmmmmmy/:://///////////////////+++oydmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmdyhmNNNNNNNNNNNNmmmmNNmmy/.      `.://///-````````..-:::://-``     
 -hmmmmmmy/::///////////////////////+++osyddmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmNh:./ydmNNNNNNNNNmmmNNmo:.`         `-:/++/-``       ````..-.`      
 .hNNmdmmh+///////////////+++oo++++/////+++osyhddmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmNd:` `-:/+syhdmmmNNNmd+. `           `.-/+/+:.`                     
 .sNNmddmdo/////////////////+ossyysooo++//////++oshmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmNh-        ``.-::/+/:-` `             ``.://+/:.`                   
 `/mNNNmmmy+/////////////////++ossyyhdhysoo+++////odNmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmNy.                `                     `-:/++/:.                  
 -hNNNNNNmy+//////////////////+++osyhdmmmmdhyso+oymmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmNd+`                                    `` ``.://+:`                 
 `:hmNNNmmh+:://////////++++++////+++sydmmNmNNmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmNd+.                                          ``.-/:.                 
 `.:///:.``.:///////////+osssso++/+++++oyhddhhdmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmNmh/`                                              ```                  
 `-////////////+osyyyyso+/////////+shNmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmNmmNmmmy:`                                                                    
 .://///+++++++++osyyhyso++++++/+sdNNNNNNNNmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmdo.                                                                      
 `.-/+//+ossysso+++oosyhhddddddddmNNNNNNNNmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmNds:.`                                                                       
 `.-:+/+oossyyyso++++oosyhdmNNmmmmmmmmmmmmmmmNNNNNNNNNNNNNmmmmmmmmmmmmmmmmmmmmmmmNd/`                                                                         
 ``--:++oossyhdhso++//+oshmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmNNy-                                                                         
 `/o/-/++osyhddhs/::::odmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmNd/                                                                         
 -y+`.+hs++++osyhhs++ohmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmNmo`                                                                        
 `++. .oNNmho///++ymmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmNs.                                                                        
 `-s/  -yNNmmmhsoosdmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmNd:                                                                        
 `/+. `+mNmmmmmNmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmNd+`                                                                       
 .o-  .ommmmmmmmmmmNmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmNmo`                                                                       
 .:`  `+mNmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmhs+-                                                                        
 /dNmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmNms.``                                                                         
 `     .+ydmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmdddddmms.                                                                           
 ```.+hddmddmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmNmmmmmmmmmmmmmmmddddddddddy:` `                                                                         
 ``:ydddddddddddddddddddddddddmmmddmmhhmNmmmmmmmmmmmmmmmmmmmddddddmdo`                                                                            
 `:ydddddddddddddddddddddddddmmmdmh/-odmmmmmmmmmmmmmmmmdddddddddmh:`                                                                            
 /hmddddddddddddddddddddddmmddmmo. .smmmmmmmmmmmmmmdddddddddddds.                                                                             
 `/dddddddddddddddddddddddddddmh/` `/dNmmmmmmmmmmmdddddddddddmh/`                                                                             
 .+ddddddddddddddddddddddddddmy.   .yNmmmmmmmmmmmdddddddddddmh:                                                                              
 -hmdddddddddddddddddddddddmd+`   `omNmmmmmmmmmmmdddddddddddy-                                                                              
 .ymdddddddddddddddddddddddmh:    -yNmmmmmmmmmmmdddddmmddddds`                                                                              
 .oddddddddddddddddddddddddmy- ``-smNNmmmmmmmmmmdddddddddddh/                                                                               
 .sddddddddddddddddddddddmdmy::oyhmNNmmmmmmmmmmmdddddddddddo`                                                                               
 `/hdddddddddddddddddddddddmd++dNNNNmmmmmmNmmmmmmdddddddddmh+`                                                                               
 `---+ymdddddddddddddddddddddddmy-.yNNNNmNNNmmmmmmmmmddddddmdmmmmy+-`                                                                            
 -hmmmmmddddddddddddddddddddddmmy:`/hNMNNNNNNmmmmmmmmmmmmmmmmmmNNMmo.                                                                            
 .sNMNmmmmmdddddddddddddddddddmmmds+ymNNNNNNNNNNNNNNNNNNNNNNNNNNNNy-  `                                                                          
 -hMNNNmmmmmmmmmmmmmmmmmmmmmmmNNNNNddNNNMNNNNMNNNNNNNNNNNNNNNNNNs.                                                                              
 `+mNNNmNNNNNNNmmmNNNNNNNNNNNNNNNNh::dNNNMNNNNNNNNNNNNNNNNNNNNNy.                                                                               
 -hNNNmNNNNNNNNNNNNNNNNNNNNNNNNMd:``oNNNNNNNNNNNNNNNNNNNNNNNNh:                                                                                
 `+mMNNNNNNNNNNNNNNNNNNNNNNNNNNm+`  -hNNNNNNNNNNNNNNNNNNNNNMd:`                                                                                
 -yMNNNNNNNNNNNNNNNNNNNNNNNNMmo`   `omMNNNNNNNNNNNNNNNNNNNm/`                                                                                 
 ``````````` `+mNNmNNNNNNNNNNNNNNNNNNNNNNs-`..``:dMNNNNNNNNNNNNNNNNNNm+`                                                                                  
 ```````...........:yMNNNNNNNNNNNNNNNNNNNNNNNy:.....-:hMNNNNNNNNNNNNNNNNNNs.``  `````     `  ````                                                              
 ````.....-----------+mNNNNNNNNNNNNNNNNNNNNNNh:.------:yNNNNNNNNNNNNNNNNNNm+.``````````````````````````                                                         
 ```......-----------:yNNNNNNNNNNNNNNNNNNNNNd/-.--::--:yNMNNNNNNNNNNNNNNNNh/-..``````.`.......```.``````````                                                    
 ````....-------::----sNNNNNNNNNNNNNNNNNNNMm+..--::--:odNNNNNNNNNNNNNNNNNNdo-......................`````````````                                                
 `````````````````````````          ````...----:::::::::--omNNNNNNNNNNNNNNNNNNNh:..-----/ymMMNNNNNNNNNNNNNNNNNMmh+.`........................``````````````                                          
 ````````````````````````````````````....----:::::::/::/:-+mNNNNNNNNNNNNNNNNNMmo-.----:/sNMNNNNNNNNNNNNNNNNNNNNNNNdo:............................````````````````````````                           
 ````................``````````````....----:::::////////::omNNNNNNNNNNNNNNNNNMm+.----::+dNNNNNNNNNNNNNNNNNNNNNNNNNNNmhs+/:-...............................```````````````````                       
 `````.............................`...-----::::////////:/smNNNNNNNNNNNNNNNNNNNNds/:::::odNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNmdhso/-........................................``````````````               
 ````................................------::::://///::+hNNNNNNNNNNNNNNNNNNNNNMMNmh+:::+hNMNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNd+-.........................................````````````              
 ````...........-...................--------::::::::::::yNNNNNNNNNNNNNNNNNNNNNNNNNNNmho//yNMNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNmo-...................................................```            
 ````..---------------------------------:---::::::-----:hMNNNNNNNNNNNNNNNNNNNNNNNNNNNNNmdmNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNh/`.-..------------------............................```            
 ````....--------------------------------------------....:hMNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNmo...---------------------..........................````            
 ```....---------------------------------------------......yNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNh/..---------------------..........................``````````````  
 `......----------------------------------------------.....`omNmNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNmyoo+oymNNNNNNNNNNNNNNNNNNNNNNNNNMmo..--------------::-----..........................````````````````
 `.......-----------------------------::::------------.....`/dNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNy-...-/ymNMNNNNNNNNNNNNNNNNNNNNNMm+..-------::::::::::----.......................................```
 `.........----------:-:::::::::::::::::::::----------.....`:yNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNd/......:smNNNNNNNNNNNNNNNNNNNNNmh/..---::::::::::::::----.......................................```
 `..............----:::::::::::::::::::::::::::::------......-+shddmNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNs-.......:/++osyhddmmmmmmdddhs+:-.----:::::::::::::::-----......................................```
 `..............----:::::::::::::::::::::::::::::------..........-:/+oydNNNNNNNNNNNNNNNNNNNNNNNNNNNNNMh/..............---:::::::--......----::::::::::::::::----......................................```
 `..............----::::::::::::::::::::::::::::------................-+dNNNNNNNNNNNNNNNNNNNNNNNNNNNNNh/.................................---::::::::::::::::----.......................................``
 `...............------------::::--------------------....-..............:ohmNNNNNNNNNNNNNNNNNNNNNmNNMNy:.................................---------:::---::------.......................................``
 `.................----------------------------------......................:+shddmmNNNNNNNNNNNNNNNmdyo:.................................-----------------------........................................``
 ```````````````...------------------..........................................-/+oossssssssssoo+/-.`.......................................--------.................................................```
 ````..............................................................................................................................................````````.........````````````````````````
 ```````................................................................................................................`````````````````````` ```````````````````````````````````````````
 `````````````.......................................................................................................```````````````````````````````````````````                       
 ````````````````````````````````````````````````````````````````````````````````````````````````````````````                               
 */








function Qid(string) {
    var ret = document.getElementById(string);
    if (ret === null) {
        console.error("ERROR : object with Id : '" + string + "' not found");
    }
    return ret;
}


class oeuvre {
    constructor(canvas) {
        this.canvas = canvas;
        this.context = canvas.getContext("2d");
    }

    getContext() {
        return this.context;
    }

    getCanvas() {
        return this.canvas;
    }

    getPixelValueAt(left, top, value) {
        var imageData = this.context.getImageData(left, top, 1, 1).data;
        switch (value) {
            case 'r':
                return imageData[0];
                break;
            case 'g':
                return imageData[1];
                break;
            case 'b':
                return imageData[2];
                break;
            case 'a':
                return imageData[3];
                break;
        }
    }

    pickColor(event) {
        if (isPicking) {
            var r, g, b, a;
            var bodyRect = document.body.getBoundingClientRect(),
                    elemRect = this.canvas.getBoundingClientRect(),
                    offsetX = elemRect.left - bodyRect.left,
                    offsetY = elemRect.top - bodyRect.top;
            r = this.getPixelValueAt(event.clientX - offsetX, event.clientY - offsetY, 'r');
            g = this.getPixelValueAt(event.clientX - offsetX, event.clientY - offsetY, 'g');
            b = this.getPixelValueAt(event.clientX - offsetX, event.clientY - offsetY, 'b');
            a = this.getPixelValueAt(event.clientX - offsetX, event.clientY - offsetY, 'a');
            color = "#" + r.toString(16) + g.toString(16) + b.toString(16) + a.toString(16);
            Qid("color-picked").style = "background-color: " + color + ";";
            actualiseExemple();
        }
    }

    convertTextareaToCanvas(txtarea) {
        var canvas = document.createElement("canvas");
        canvas.width = txtarea.clientWidth + 10;
        canvas.height = txtarea.clientHeight;
        var ctx = canvas.getContext("2d");
        var rgba = txtarea.style.backgroundColor.replace("rgba(", "").replace(")").split(',');
        var r = parseInt(rgba[0]);
        var g = parseInt(rgba[1]);
        var b = parseInt(rgba[2]);
        var a = parseFloat(rgba[3]) * 255;
        var canvasoeuvre = new oeuvre(canvas);
        for (var i = 0; i < canvas.width; i++) {
            for (var i2 = 0; i2 < canvas.height; i2++) {
                canvasoeuvre.setPixelValueAt(ctx, i, i2, r, g, b, a);
            }
        }
        var str = txtarea.value;
        ctx.fillStyle = txtarea.style.color;
        ctx.font = (txtarea.style.fontSize.replace("px", "") - 10) + 'pt ' + txtarea.style.fontFamily;
        ctx.fillText(str, canvas.width / 2 - ctx.measureText(str).width / 2, 45);
        this.context.drawImage(canvasoeuvre.getCanvas(), 0, parseInt(txtarea.style.top.replace("px", "")) - canvasY);
    }

    comparePixelData(left, top, r, g, b, a) {
        var ret = false;
        if (this.getPixelValueAt(left, top, 'r') === r && getPixelValueAt(left, top, 'g') === g && getPixelValueAt(left, top, 'b') === b && getPixelValueAt(left, top, 'a') === a) {
            ret = true;
        }
        return ret;
    }

    setPixelValueAt(left, top, r, g, b, a) {
        var imageData = this.getImageData(left, top, 1, 1);
        imageData.data[0] = r;
        imageData.data[1] = g;
        imageData.data[2] = b;
        imageData.data[3] = a;
        this.putImageData(imageData, left, top);
    }

    traceLigne(xa, ya, xb, yb, R, G, B, A) {
        var a;
        var c;
        var yi;
        var xi;
        function colorPixel() {
            if (!(xi > LF || yi > HF || xi < 0 || yi < 0)) {
                if (!this.comparePixelData(xi, yi, R, G, B, A)) {
                    this.setPixelValueAt(xi, yi, R, G, B, A);
                }
            }
        }
        if (xb === xa) {
            a = 9999999;
        } else {
            a = (yb - ya) / (1. * xb - xa);
        }
        c = ya - a * xa;

        if (a >= -1 || a <= 1) {
            if (xa <= xb) {
                for (xi = xa; xi < xb; xi++) {
                    yi = a * xi + c;
                    colorPixel();
                }
            }
            if (xa > xb) {
                for (xi = xa; xi > xb; xi--) {
                    yi = a * xi + c;
                    colorPixel();
                }
            }
        }
        if ((a < -1. || a > 1.) && a !== 9999999) {
            if (ya <= yb) {
                for (yi = ya; yi < yb; yi++) {
                    xi = (yi - c) * 1. / a;
                    colorPixel();
                }
            }
            if (ya > yb) {
                for (yi = ya; yi > yb; yi--) {
                    xi = (yi - c) * 1. / a;
                    colorPixel();
                }
            }
        }
        if (a === 9999999) {
            if (ya <= yb) {
                for (yi = ya; yi < yb; yi++) {
                    xi = xa;
                    colorPixel();
                }
            }
            if (ya > yb) {
                for (yi = ya; yi > yb; yi--) {
                    xi = xa;
                    colorPixel();
                }
            }
        }
    }

    traceLigneShape(xa, ya, xb, yb, R, G, B, A, func) {
        var a;
        var c;
        var yi;
        var xi;
        function colorPixel() {
            if (!(xi > LF || yi > HF || xi < 0 || yi < 0)) {
                func(this.ctx, xi, yi, R, G, B, A);
            }
        }
        if (xb === xa) {
            a = 9999999;
        } else {
            a = (yb - ya) / (1. * xb - xa);
        }
        c = ya - a * xa;

        if (a >= -1 || a <= 1) {
            if (xa <= xb) {
                for (xi = xa; xi < xb; xi++) {
                    yi = a * xi + c;
                    colorPixel();
                }
            }
            if (xa > xb) {
                for (xi = xa; xi > xb; xi--) {
                    yi = a * xi + c;
                    colorPixel();
                }
            }
        }
        if ((a < -1. || a > 1.) && a !== 9999999) {
            if (ya <= yb) {
                for (yi = ya; yi < yb; yi++) {
                    xi = (yi - c) * 1. / a;
                    colorPixel();
                }
            }
            if (ya > yb) {
                for (yi = ya; yi > yb; yi--) {
                    xi = (yi - c) * 1. / a;
                    colorPixel();
                }
            }
        }
        if (a === 9999999) {
            if (ya <= yb) {
                for (yi = ya; yi < yb; yi++) {
                    xi = xa;
                    colorPixel();
                }
            }
            if (ya > yb) {
                for (yi = ya; yi > yb; yi--) {
                    xi = xa;
                    colorPixel();
                }
            }
        }
    }

    traceCarre(xa, ya, ep, r, g, b, a) {
        var xb = xa + ep;
        var yb = ya + ep;

        for (var i = xa; i < xb; i++) {
            this.setPixelValueAt(i, ya, r, g, b, a);
        }
        for (var i2 = ya; i2 < yb; i2++) {
            this.setPixelValueAt(xa, i2, r, g, b, a);
        }
        for (var i = xa; i < xb; i++) {
            this.setPixelValueAt(i, yb, r, g, b, a);
        }
        for (var i2 = ya; i2 < yb; i2++) {
            this.setPixelValueAt(xb, i2, r, g, b, a);
        }

        /*for (var i = xa; i < xb; i++) {
         for (var i2 = ya; i2 < yb; i2++) {
         setPixelValueAt(ctx, i, i2, r, g, b, a);
         }
         }*/

        /*traceLigne(ctx,xa,ya,xa,yb,r,g,b,a);
         traceLigne(ctx,xa,yb,xb,yb,r,g,b,a);
         traceLigne(ctx,xb,yb,xb,ya,r,g,b,a);
         traceLigne(ctx,xb,ya,xa,ya,r,g,b,a);
         autoFill(ctx,(xa+xb)/2,(ya+yb)/2,r,g,b,a);*/
    }

    autoFill(x, y, r, g, b, a) { //Fonction de remplissage
        if ((x >= 0 && x <= LF) && (y >= 0 && y <= HF)) {
            this.setPixelValueAt(x, y, r, g, b, a);
            if (this.comparePixelData(x + 1, y, 0, 0, 0, 0) && (testFreeSpaceXp(this.context, x + 1, y) === true)) {
                this.autoFill(x + 1, y, r, g, b, a);
            }
            if (this.comparePixelData(x - 1, y, 0, 0, 0, 0) && (testFreeSpaceXn(this.context, x - 1, y) === true)) {
                this.autoFill(x - 1, y, r, g, b, a);
            }
            if (this.comparePixelData(x, y + 1, 0, 0, 0, 0) && (testFreeSpaceYp(this.context, x, y + 1) === true)) {
                this.autoFill(x, y + 1, r, g, b, a);
            }
            if (this.comparePixelData(x, y - 1, 0, 0, 0, 0) && (testFreeSpaceYn(this.context, x, y - 1) === true)) {
                this.autoFill(x, y - 1, r, g, b, a);
            }
        }
    }

    traceCerclePixels(xT, yT, ep, r, g, b, a) {
        var canvas = document.createElement("canvas");
        canvas.width = 2 * ep + 2;
        canvas.height = 2 * ep + 2;
        var ctxCanvas = canvas.getContext("2d");
        var y, y2;
        var xp;
        var yp, yp2;
        var xpAnci = -1;
        var ypAnci = -1;
        var yp2Anci = -1;
        var canvasoeuvre = new oeuvre (canvas);
        for (var i = -2; i < 2; i = i + 0.1) {
            y = Math.sqrt(4 - Math.pow(i, 2));
            y2 = -Math.sqrt(4 - Math.pow(i, 2));
            xp = ((ep + 1) - ep) + ((i + 2) * (((ep + 1) + ep) - ((ep + 1) - ep)) / (2 + 2));
            yp = ((ep + 1)) + ((y) * (((ep + 1) + ep) - ((ep + 1))) / (2));
            yp2 = ((ep + 1) - ep) + ((y2 + 2) * (((ep + 1)) - ((ep + 1) - ep)) / (2));
            if (xpAnci !== -1) {
                canvasoeuvre.traceLigne(xpAnci, ypAnci, xp, yp, r, g, b, a);
                canvasoeuvre.traceLigne(xpAnci, yp2Anci, xp, yp2, r, g, b, a);
            }
            xpAnci = xp;
            ypAnci = yp;
            yp2Anci = yp2;
        }
        xp = ((ep + 1) - ep) + ((2 + 2) * (((ep + 1) + ep) - ((ep + 1) - ep)) / (2 + 2));
        yp = ((ep + 1)) + ((0) * (((ep + 1) + ep) - ((ep + 1))) / (2));
        yp2 = ((ep + 1) - ep) + ((0 + 2) * (((ep + 1)) - ((ep + 1) - ep)) / (2));
        if (xpAnci !== -1) {
            canvasoeuvre.traceLigne(xpAnci, ypAnci, xp, yp, r, g, b, a);
            canvasoeuvre.traceLigne(xpAnci, yp2Anci, xp, yp2, r, g, b, a);
        }
        canvasoeuvre.setPixelValueAt(ep + 1 + ep, ep + 1, r, g, b, a);
        for (var i = 0; i < (2 * ep + 2); i++) {
            for (var i2 = 0; i2 < (2 * ep + 2); i2++) {
                if (comparePixelData(ctxCanvas, i, i2, r, g, b, a) && (xT - ep + i > 0 && xT - ep + i < LF && yT - ep + i2 > 0 && yT - ep + i2 < HF)) {
                    var tr = getPixelValueAt(ctxCanvas, i, i2, 'r');
                    var tg = getPixelValueAt(ctxCanvas, i, i2, 'g');
                    var tb = getPixelValueAt(ctxCanvas, i, i2, 'b');
                    var ta = getPixelValueAt(ctxCanvas, i, i2, 'a');
                    this.setPixelValueAt(xT - ep + i, yT - ep + i2, tr, tg, tb, ta);
                }
            }
        }
    }
}