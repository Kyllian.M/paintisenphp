<?php
    session_start();
    function chargerClasse($classe){
        require $classe.'.php';
	}
	spl_autoload_register('chargerClasse');
	chargerClasse('BddManager');
	$dataBase = new BddManager('localhost','chris','53132834353',"Paint'ISEN");
	
	if(isset($_POST['stop_follow'])){
        $dataBase->removeFollower(htmlspecialchars($_POST['stop_follow']),$_SESSION['user_id']);
    }
    else if(isset($_POST['follow'])){
        $dataBase->addFollower(htmlspecialchars($_POST['follow']), $_SESSION['user_id']);
    }
    else if(isset($_POST['up'])){
        $dataBase->manageLike($_POST['up'],$_SESSION['user_id']);
    }
    else if(isset($_POST['delcom'])){
        $dataBase->removeCom($_POST['delcom']);
    }
    else if(isset($_POST['delpost'])){
        $dataBase->removePost($_POST['delpost']);
    }
    else if(isset($_POST['state_moduler'])){
        $dataBase->stateModuler($_POST['state_moduler']);
    }
?>
