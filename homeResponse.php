<!DOCTYPE html>
<html lang=fr>
    <head>
        <meta charset=utf-8 />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="style.css"/>
        <link rel="stylesheet" href="article.css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
            function hideIcon(self) {
                if (self.value !== "") {
                    self.style.backgroundImage = 'none';
                } else {
                    self.style.background = "url('searchbar.png')";
                }
            }
            function stop_follow(username) {
                var http = new XMLHttpRequest();
                http.open("POST", "abonnements.php", true);
                http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                var params = "stop_follow=" + username;
                http.send(params);
                http.onload = function () {
                    $("aside").load(location.href + " aside", "");
                    $("#posts").load(location.href + " #posts", "");
                };
            }
            function follow(username) {
                var http = new XMLHttpRequest();
                http.open("POST", "abonnements.php", true);
                http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                var params = "follow=" + username;
                http.send(params);
                http.onload = function () {
                    $("aside").load(location.href + " aside", "").fadeIn('normal');
                    $("#posts").load(location.href + " #posts", "").fadeIn('normal');
                };
            }
            function form_like(object_id, username) {
                var http = new XMLHttpRequest();
                http.open("POST", "abonnements.php", true);
                http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                var params = "up=" + object_id;
                http.send(params);
                http.onload = function () {
                    $("#like_dislike_" + object_id).load(location.href + " #like_dislike_" + object_id + ">*", "").fadeIn('normal');
                    $("#article_aside_content_comment_" + object_id).load(location.href + " #article_aside_content_comment_" + object_id, "").fadeIn('normal');
                };
            }
            function addcom(id_post) {
                var http = new XMLHttpRequest();
                http.open("POST", "profil.php", true);
                http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                var params = "comment_content=" + $("textarea#comment_content_" + id_post).val() + "&id_post=" + id_post;
                http.send(params);
                http.onload = function () {
                    $("#article_aside_content_comments_" + id_post).load(location.href + " #article_aside_content_comments_" + id_post + ">*", "");
                }
            }
            function responsive_menu() {
                if (document.getElementById('menu_right_options_responsive_profil').style.display !== 'block' || document.getElementById('menu_right_options_responsive_settings').style.display !== 'block' || document.getElementById('menu_right_options_responsive_logout').style.display !== 'block') {
                    document.getElementById('menu_right_options_responsive_profil').style.display = 'block';
                    document.getElementById('menu_right_options_responsive_settings').style.display = 'block';
                    document.getElementById('menu_right_options_responsive_logout').style.display = 'block';
                } else {
                    document.getElementById('menu_right_options_responsive_profil').style.display = 'none';
                    document.getElementById('menu_right_options_responsive_settings').style.display = 'none';
                    document.getElementById('menu_right_options_responsive_logout').style.display = 'none';
                }
            }
            function delcom(id_com) {
                var http = new XMLHttpRequest();
                http.open("POST", "abonnements.php", true);
                http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                var params = "delcom=" + id_com;
                http.send(params);
                http.onload = function () {
                    document.getElementById('article_aside_content_comment_' + id_com).classList.add('hide');
                };
            }
        </script>
        <title>Paint'ISEN</title>
    </head>
    <body>
        <header>
            <div id="menu">
                <table id="menu_left_options">
                    <tr>
                        <td id="subscriptions_button_container" class="begining_color">
                            <div id ="subscriptions_button_container2"><button type="button" onclick="document.getElementById('rand').classList.add('hide');
                                    document.getElementById('link').classList.add('hide');document.getElementById('subscriptions').classList.remove('hide');
                                    document.getElementById('subscriptions_button').style.backgroundColor = 'DarkGray ';
                                    document.getElementById('subscriptions_button').style.borderRadius = '5px';
                                    document.getElementById('link_button').style.backgroundColor = 'transparent';
                                    document.getElementById('rand_button').style.backgroundColor = 'transparent';
                                    " id="subscriptions_button"><img alt="SocialIcon" src="socialicon.png"/></button></div>
                        </td>
                        <td id="rand_button_container">
                            <div><button type="button" onclick="document.getElementById('subscriptions').classList.add('hide');
                                    document.getElementById('link').classList.add('hide');
                                    document.getElementById('rand').classList.remove('hide');
                                    document.getElementById('rand_button').style.backgroundColor = 'DarkGray ';document.getElementById('rand_button').style.borderRadius = '5px'; document.getElementById('link_button').style.backgroundColor = 'transparent';document.getElementById('subscriptions_button').style.backgroundColor = 'transparent';
                                    document.getElementById('subscriptions_button_container').classList.remove('begining_color');" id="rand_button"><img alt="NotificationIcon" src="missingicon.png"/></button></div>
                        </td>
                        <th id="link_button_container">
                            <div><button type="button" onclick="document.getElementById('subscriptions').classList.add('hide');
                                    document.getElementById('rand').classList.add('hide');
                                    document.getElementById('link').classList.remove('hide');
                                    document.getElementById('link_button').style.backgroundColor = 'DarkGray ';
                                    document.getElementById('link_button').style.borderRadius = '5px';
                                    document.getElementById('rand_button').style.backgroundColor = 'transparent';document.getElementById('subscriptions_button').style.backgroundColor = 'transparent';
                                    document.getElementById('subscriptions_button_container').classList.remove('begining_color');
                                    " id="link_button"><img alt="RecommandationIcon" src="missingicon.png"/></button></div>
                        </th>
                    </tr>
                </table>
                <div id="menu_center_options">
                    <table>
                        <tr>
                            <th>
                                <div>
                                    <a href="home.php">
                                        <img alt="Accueil" src="logo.png" id="menu_center_logo" title="Accueil"/>
                                    </a>
                                </div>
                            </th>
                            <th>
                                <div>
                                    <form method="POST" action="#">
                                        <input name="searchbar" size="30" id="menu_center_search" type="text" onchange="hideIcon(this);" style="text-align:center;"/>
                                    </form>
                                </div>
                            </th>
                            <th>
                                <div>
                                    <table id= "menu_right_options_responsive">
                                        <tr>
                                            <th>
                                                <button id="menu_right_options_responsive_button" type="button" onclick="responsive_menu()"><img alt="Menu" src="menu.png"/></button>
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="index.php" id="menu_right_options_responsive_settings">
                                                    <img alt="Dessiner" src="pencil.png" title="Dessiner"/>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <script style="display:none;">
                                                    function form_link_<?php INSERT HERE ?>() {
                                                        document.profil_link_<?php INSERT HERE ?>.submit();
                                                    }</script>
                                                <form name="profil_link_<?php INSERT HERE ?>" method="POST" style="display:inline;" action="profil.php">
                                                    <input name="profil_link" type="hidden" value="
                                                           <?php INSERT HERE ?>"/>
                                                    <a href="javascript: form_link_<?php INSERT HERE ?>()" id="menu_right_options_responsive_profil">
                                                        <img alt="Profil" src="profil.png" title="Profil"/>
                                                    </a>
                                                </form>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>
                                                <a href="goodbye.php" id="menu_right_options_responsive_logout">
                                                    <img alt="Déconnexion" src="logout.png" title="Déconnexion"/>
                                                </a>
                                            </th>
                                        </tr>
                                    </table>
                                </div>
                            </th>
                        </tr>
                    </table>
                </div>
                <video style="display:none;" muted autoplay><source src= "goodbye.mp4"></video>
                <div id="menu_right_options">
                    <div id="normal_menu_right_options">
                        <div>
                            <a href="Editor/index.php">
                                <img alt="Dessiner" src="pencil.png" title="Dessiner"/>
                            </a>
                        </div>
                        <div>
                            <script style="display:none;">
                                function form_link_<?php INSERT HERE ?>() {
                                    document.profil_link_<?php INSERT HERE ?>.submit();
                                }</script>
                            <form name="profil_link_<?php INSERT HERE ?>" method="POST" style="display:inline;" action="profil.php">
                                <input name="profil_link" type="hidden" value="<?php INSERT HERE ?>"/>
                                <a href="javascript: form_link_<?php INSERT HERE ?>()">
                                    <img alt="Profil" src="profil.png" title="Profil"/>
                                </a>
                            </form>
                        </div>
                        <div>
                            <a href="goodbye.php">
                                <img alt="Déconnexion" src="logout.png" title="Déconnexion"/>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <aside>
            <div id="subscriptions">
                <h1>
                    Abonnements
                </h1>
                <br/>
                <div id="sub_content">
                    <?php INSERT HERE ?>
                    </div>
            </div>
            <div id="rand" class="hide">
                <h1>
                    Notifications
                </h1>
                <br/>
            </div>
            <div id="link" class="hide">
                <h1>
                    Recommandations
                </h1>
                <br/>
            </div>
        </aside>
        <div id="main_content">
            <section id="daily_message">
                <h1>
                    <?php INSERT HERE ?>, comment allez-vous aujourd'hui ?
                </h1>
            </section>
            <section id="daily_top">
                <h2 id="daily_top_title">
                    Nous vous avons sélectionné les 10 images les plus populaires du jour
                </h2>
                <br/>
                <div id="daily_top_photos">
                    <?php INSERT HERE ?>
                    </div>
            </section>
            <section id="posts">
                <h2 id="posts_title">
                    Fil d'actualités
                </h2>
                <br/>
                <br/>
                <?php INSERT HERE ?>

            </section>
            <script>
                var offset = 5;
                
                function getDocHeight() {
                    var D = document;
                    return Math.max(
                        D.body.scrollHeight, D.documentElement.scrollHeight,
                        D.body.offsetHeight, D.documentElement.offsetHeight,
                        D.body.clientHeight, D.documentElement.clientHeight
                    );
                }
                
                function amountscrolled(){
                    var winheight= window.innerHeight || (document.documentElement || document.body).clientHeight;
                    var docheight = getDocHeight();
                    var scrollTop = window.pageYOffset || (document.documentElement || document.body.parentNode || document.body).scrollTop;
                    var trackLength = docheight - winheight;
                    var pctScrolled = Math.floor(scrollTop/trackLength * 100);
                    return pctScrolled;
                }
                
                function getXMLHttpRequest() {
                    var xhr = null;
                    if (window.XMLHttpRequest || window.ActiveXObject) {
                        if (window.ActiveXOject) {
                            try {
                                xhr = new ActiveXObject("Msxml2.XMLHTTP");
                            } catch (error) {
                                xhr = new ActiveXObject("Microsoft.XMLHTTP");
                            }
                        } else {
                            xhr = new XMLHttpRequest();
                        }
                    } else {
                        alert("Voici un vrai navigateur https://www.google.com/chrome/");
                        return null;
                    }
                    return xhr;
                }

                function addNewContent() {
                    var ajax = getXMLHttpRequest();
                    ajax.open("POST", "contentRequest.php", false);
                    ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                    ajax.onreadystatechange = function () {
                        offset += 5;
                        var div = document.createElement('div');
                        div.innerHTML = ajax.responseText;
                        document.querySelector("#posts").appendChild(div);
                    };
                    if(imgSearch === null){
                        ajax.send("offset=" + offset);
                    }
                    else{
                        ajax.send("offset=" + offset + "&imgSearch=" + imgSearch);
                    }
                }

                window.addEventListener("scroll", function(){
                    if(amountscrolled() >= 95){
                        addNewContent();
                    }
                }, false);
            </script>
        </div>
        <footer>
            © 2018 Paint'ISEN - Projet WebDev réalisé par VOLTO Christophe et MARIE Kyllian
        </footer>
    </body>
</html>