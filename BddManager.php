<?php
	
    class BddManager{
        private $stream; //Objet PDO
        
        function __construct($server_host,$server_login,$server_password,$DBname){ //Connection à la BDD
            
            try{
                $this->stream = new PDO('mysql:host='.$server_host.';dbname='.$DBname.';charset=utf8','root');
                $this->stream ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
            catch(PDOException $e){
            }
        }
        
        public function addUser($login,$password,$username){ //Ajoute un utilisateur à la BDD
            $message = array("red");
            if(strlen($login) >=4 && strlen($login) <=65 && strlen($password) >=5 && strlen($password) <=65 && strlen($username) >=2 && strlen($username) <=20 && filter_var($username, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^[a-zA-Z][a-zA-Z0-9\s]*$/")))){
                $login_already_taken = false;
                foreach($this->stream->query('SELECT user_login FROM authentification') as $data){
                    if ($login == $data['user_login']){
                        $login_already_taken = true;
                        break;
                    }
                }
                if(!$login_already_taken){
                    $username_already_taken = false;
                    foreach($this->stream->query('SELECT username FROM profils') as $data){
                        if ($username == $data['username']){
                            $username_already_taken = true;
                            break;
                        }
                    }
                    if(!$username_already_taken){
                        
                        try{
                            $password = sha1($password);
                            $date = date('Y-m-d H:i:s');
                            $login = htmlspecialchars($login);
                            $password = htmlspecialchars($password);
                            $username = htmlspecialchars($username);
                            $this->stream->exec("INSERT INTO authentification VALUES ('$login','$password',Null)");
                            foreach  ($this->stream->query("SELECT `user_id` FROM `authentification` WHERE `user_login`='$login' AND `user_password`='$password'") as $data) {
                                $this->stream->exec('INSERT INTO profils VALUES (\''.$username.'\',\''.$data['user_id'].'\',DEFAULT,DEFAULT,\''.$date.'\')');
                            }
                            $message = array("blue","Inscription réussie","Login: <span style='color:red;'>$login</span>","Mot de passe: Inaccessible (encodé en sha1 à l'arrivée du formulaire au serveur)","Nom: <span style='color:red;'>$username</span>");
                        }catch(PDOException $e){
                            array_push($message,"Inscription échouée. Reessayez plus tard.");
                        }
                    }
                    else{
                        array_push($message,"Le pseudonym $username est déjà pris");
                    }
                }
                else{
                    array_push($message,"Le login $login est déjà pris");
                }
            }
            else{
                array_push($message,"Essayer de modifier la page ne mène à rien de bon!");
            }
            return $message;
        }
        public function connectUser($login,$password){ //Système de connection qui renvoie l'ID d'un utilisateur s'il existe
            if(strlen($login) >=4 && strlen($login) <=65 && strlen($password) >=5 && strlen($password) <=65){
                $password = sha1($password);
                $user_id = 0;
                $request = "SELECT * FROM authentification";
                try{
                    if($this->stream->query($request)){
                        foreach($this->stream->query($request) as $data){
                            if($data['user_login']==$login && $data['user_password'] == $password){
                                $user_id = $data['user_id'];
                                echo "<script>alert('ok')</script>";
                            }
                        }
                    }
                    return $user_id;
                }catch(PDOException $e){
                    echo "Connexion échouée. Reessayez plus tard.";
                }
            }
        }
        public function buildUser($user_id,$username=null){ //Renvoie un tableau avec les informations de l'utilisateur, prend en param soit son ID soit son pseudonyme
            if($username==null){
                $request = "SELECT * FROM profils WHERE user_id = $user_id";
                try{
                    if($this->stream->query($request)){
                        foreach($this->stream->query($request) as $data){
                            return $data;
                        }
                    }
                }catch(PDOException $e){
                }
            }else{
                $request = "SELECT * FROM profils WHERE username = '$username'";
                try{
                    if($this->stream->query($request)){
                        foreach($this->stream->query($request) as $data){
                            return $data;
                        }
                    }
                }catch(PDOException $e){
                }
            }
        }
        public function putCookie($user_id,$user_ip){ //Envoi un cookie et stocke l'id du cookie; l'ip de l'utilisateur et son ID dans la BDD
            try{
                $this->stream->exec("DELETE FROM `cookies` WHERE user_id=$user_id");
                $id_cookie = sha1(rand(0,9999999999999999));
                $ttl = time()+3600*24*30;
                $this->stream->exec("INSERT INTO cookies VALUES ('$id_cookie','$user_ip','$user_id','$ttl')");
                setcookie("id_cookie","$id_cookie",$ttl, null, null, false, true);
            }catch(PDOException $e){
            }
        }
        public function destroyCookie($user_id){ //Détruit un cookie de la BDD
            try{
                $this->stream->exec("DELETE FROM `cookies` WHERE user_id=$user_id");
            }catch(PDOException $e){
            }
        }
        public function updateUser($user){ //Met à jour les informations d'un utilisateur grâce à l'objet User (se référer à User.php)
            $this->stream->exec('UPDATE `profils` SET `username`="'.$user->getUsername().'",`profil_image`="'.$user->getProfil_image().'",`user_permissions`="'.$user->getUser_permissions().'" WHERE user_id='.$user->getUser_id().'');
        }
        public function changeUserPass($password,$user_id){ //Modifie le mot de passe d'un utilisateur
            $password=sha1($password);
            $this->stream->exec('UPDATE `authentification` SET `user_password`="'.$password.'" WHERE user_id='.$user_id.'');
        }
        public function checkCookie($id_cookie, $user_ip){ //Vérifie si l'id du cookie et l'ip de l'utilisateur correspondent à ceux dans la table cookies
            try{
                $user_id = 0;
                $request = "SELECT * FROM cookies";
                if($this->stream->query($request)){
                    foreach($this->stream->query($request) as $data){
                        if($data['timetolive'] > time()){
                            $this->stream->exec('DELETE FROM `cookies` WHERE user_id='.$data["user_id"]);
                        }
                        if($data['id_cookie']==$id_cookie && $data['user_ip'] == $user_ip){
                            if($data['timetolive'] > time()){
                                $user_id = $data['user_id'];
                            }
                            else{
                                $this->stream->exec("DELETE FROM `cookies` WHERE user_id=$user_id");
                            }
                        }
                    }
                }
                return $user_id;
            }catch(PDOException $e){
                echo $e;
            }
        }
        public function displayFollowers($user_id){ //Affiche les abonnements de l'utilisateur
            $request = "SELECT followed FROM abonnements WHERE follower = $user_id";
            try{
                if($this->stream->query($request)){
                    
                    foreach($this->stream->query($request) as $data){
                        $rand = sha1(rand(0,9999999999999999));
                        $nbimages= $this->getNumberImages($data['followed']);
                        $nbfollowers= $this->getNumberFollowers($data['followed']);
                        $follower = $this->buildUser($user_id);
                        $followed = $this->buildUser($data['followed']);
                        
        echo '
            <div class="sub_user" id="sub_user_'.$followed['username'].'">
                <div class="sub_ico">
                    <a href="#"><img alt="ProfilImage" style="width:50px;height:50px;border-radius: 50%;" src="'.$followed['profil_image'].'"/></a>
                </div>
                <div class="sub_name">
                    <script  style="display:none;">
                    function form_link_'.$rand.'(){document.profil_link_'.$rand.'.submit();}</script>
                    <form name="profil_link_'.$rand.'" method="POST" style="display:inline;" action="profil.php">
                    <input name="profil_link" type="hidden" value="'.$followed['username'].'"/>
                    <a href="javascript: form_link_'.$rand.'()" data-toggle="tooltip" title="('.$nbimages.' images | '.$nbfollowers.' abonnés)">'.$followed['username'].'</a>
                    </form>
                </div>
                <div class="sub_link">
                        <a href="javascript: stop_follow(\''.$followed['username'].'\')">Ne plus suivre</a>
                    </form>
                </div>
            </div>
            <br/>';
                    }
                }
            }catch(PDOException $e){
            }
        }
        public function manageLike($post_id, $user_id){ //Système de like/dislike d'image/commentaire
            foreach($this->stream->query("SELECT user_id FROM ups WHERE object_id=$post_id") as $data){
                if($data['user_id']==$user_id){
                    $this->stream->exec("DELETE FROM `ups` WHERE user_id=$user_id AND object_id = $post_id");
                    
                    foreach($this->stream->query("SELECT type FROM objectsIdCreator WHERE object_id=$post_id") as $data){
                        if($data['type']==0){
                            $this->stream->exec("UPDATE `commentaire` SET `up`=up-1 WHERE comment_id=$post_id");
                        }
                        else{
                            $this->stream->exec("UPDATE `posts` SET `up`=up-1 WHERE id_post=$post_id");
                            $this->stream->exec("UPDATE `daily_top` SET `up`=up-1 WHERE object_id=$post_id");
                        }
                    }
                    $like = false;
                }
            }
            if(!isset($like)){
                $this->stream->exec("INSERT INTO ups VALUES('$post_id','$user_id')");
                foreach($this->stream->query("SELECT type FROM objectsIdCreator WHERE object_id=$post_id") as $data){
                    if($data['type']==0){
                        $this->stream->exec("UPDATE `commentaire` SET `up`=up+1 WHERE comment_id=$post_id");
                    }
                    else{
                        $this->stream->exec("UPDATE `posts` SET `up`=up+1 WHERE id_post=$post_id");
                        $this->stream->exec("UPDATE `daily_top` SET `up`=up+1 WHERE object_id=$post_id");
                    }
                }
                $like = true;
            }
        }
        public function updatePost($post_modify_id, $post_modify_description, $post_modify_title, $post_modify_private){//Met à jour les informations d'un post
            Try{
                if($post_modify_private == 'privée'){
                    $post_modify_private = 1;
                }else{
                    $post_modify_private = 0;
                }
                
                $post_modify_description = addslashes($post_modify_description);
                $post_modify_title = addslashes($post_modify_title);
                $post_modify_description = htmlspecialchars($post_modify_description);
                $post_modify_title = htmlspecialchars($post_modify_title);
                $this->stream->exec("UPDATE `posts` SET `description`='$post_modify_description',`titre`='$post_modify_title', `private`='$post_modify_private' WHERE id_post=$post_modify_id");
            }catch(PDOException $e){
                echo $e;
            }
        }
        public function checkLike($post_id, $user_id){//Vérifie si un objet est aimé par un utilisateur
            $like = false;
            foreach($this->stream->query("SELECT user_id FROM ups WHERE object_id=$post_id") as $data){
                if($data['user_id']==$user_id){
                    $like = true;
                }
            }
            return $like;
        }
        public function checkFollowed($follower_id, $followed_id){//Vérifie si une personne est suivie par une autre personne
            $link = false;
            foreach($this->stream->query("SELECT * FROM abonnements WHERE follower=$follower_id AND followed = $followed_id") as $data){
                if($data['follower'] AND $data['followed']){
                    $link = true;
                }
            }
            return $link;
        }
        public function removeFollower($followedname, $user_id){//Supprime l'abonnement d'une personne
                foreach($this->stream->query("SELECT user_id FROM profils WHERE username='$followedname'") as $data){
                    $followed_id = $data['user_id'];
                }
            Try{
                $this->stream->exec("DELETE FROM `abonnements` WHERE followed=$followed_id AND follower = $user_id");
            }catch(PDOException $e){
            }
        }
        public function getNumberFollowers($user_id){//Retourne le nombre d'abonnés d'une personne
            $number=0;
            Try{
                foreach($this->stream->query("SELECT follower FROM abonnements WHERE followed = $user_id") as $data){
                    $number=$number+1;
                }
            }catch(PDOException $e){
            }
            return $number;
        }
        public function getNumberImages($user_id){//Retourne le nombre de posts total d'une personne
            $number=0;
                foreach($this->stream->query("SELECT * FROM posts WHERE id_author='$user_id'") as $data){
                    $number=$number+1;
                }
            return $number;
        }
        public function addFollower($followedname, $user_id){//Abonne une personne à une autre
            foreach($this->stream->query("SELECT user_id FROM profils WHERE username='$followedname'") as $data){
                $followed_id = $data['user_id'];
            }
            $this->stream->exec("INSERT INTO abonnements VALUES('$user_id','$followed_id')");
        }
        public function createComment($user_id, $id_post, $comment_content){ //Création d'un commentaire
            Try{
                $rand = $rand = sha1(rand(0,9999999999999999));
                $date = date('Y-m-d H:i:s');
                $comment_content = addslashes($comment_content);
                $this->stream->exec("INSERT INTO `objectsIdCreator` VALUES(0,Null, '$rand')");
                foreach($this->stream->query("SELECT object_id FROM objectsIdCreator WHERE temp_id='$rand'") as $data){
                    $object_id = $data['object_id'];
                }
                $this->stream->exec("UPDATE objectsIdCreator SET temp_id = 0 WHERE temp_id = '$rand'");
                $this->stream->exec('INSERT INTO `commentaire` VALUES("'.$user_id.'","'.$comment_content.'","'.$date.'","0","'.$object_id.'","'.$id_post.'")');

            }catch(PDOException $e){
                echo $e;
            }
        }
        public function removePost($id_post){ //Supression de post
            Try{
                $this->stream->exec("DELETE FROM commentaire WHERE post_id = '$id_post'");
                $this->stream->exec("DELETE FROM posts WHERE id_post = '$id_post'");
                $this->stream->exec("DELETE FROM daily_top WHERE object_id = '$id_post'");
            }catch(PDOException $e){
                echo $e;
            }
        }
        public function removeCom($id_comment){ //Supression de commentaire
            Try{
                $this->stream->exec("DELETE FROM commentaire WHERE comment_id = '$id_comment'");
                $this->stream->exec("DELETE FROM ObjectsIdCreator WHERE object_id = '$id_comment'");
            }catch(PDOException $e){
                echo $e;
            }
        }
        public function getPostStatus($post_id){ //Retourne le status privé ou publique d'un post
            Try{
                foreach($this->stream->query("SELECT private FROM posts WHERE id_post=$post_id") as $data){
                    return $data['private'];
                }

            }catch(PDOException $e){
                echo $e;
            }
        }
        public function displayDailyTop(){ //Affiche le Top 10 des posts les plus aimés du jour
            Try{
                $limit =0;
                foreach($this->stream->query("SELECT object_id FROM daily_top ORDER BY up DESC") as $id){
                    if($limit <= 10){
                    $limit +=1;
                    foreach($this->stream->query('SELECT * FROM posts WHERE private=0 AND id_post='.$id['object_id'].'') as $data){
                        $image_url = $data['image'];
                        $image_title = $data['titre'];
                        $user_id = $data['id_author'];
                        foreach($this->stream->query("SELECT username FROM profils WHERE user_id=$user_id ") as $user){
                            $rand = $rand = sha1(rand(0,9999999999999999));
                            $username = $user['username'];
                            
                        echo "<figure class='daily_top_photo'>
                                <img alt=\"TopImage\" style='width:200px;height:200px;' src='$image_url'/>
                                <figcaption>
                                    $image_title
                                    <br/>
                                    <script style='display:none;'>
                                    function form_link_$rand(){document.profil_link_$rand.submit();}</script>
                                    <form name='profil_link_$rand' method='POST' style='display:inline;' action='profil.php'>
                                    <input name='profil_link' type='hidden' value='$username'/>
                                    <a href='javascript: form_link_$rand()' class='top_author'>$username</a>
                                    </form>
                                </figcaption>
                            </figure>";
                        }
                    }
                    }
                }

            }catch(PDOException $e){
                echo $e;
            }
        }
        public function stateModuler($post_id){ //Modifie le status d'un post publique à privé ou vice versa
            foreach($this->stream->query("SELECT private FROM posts WHERE id_post = $post_id") as $data){
                if($data['private']==1){
                    $this->stream->exec("UPDATE posts SET private=0 WHERE id_post=$post_id");
                }else{
                    $this->stream->exec("UPDATE posts SET private=1 WHERE id_post=$post_id");
                }
            }
        }
        public function displayComments($id_post,$user_id){//Affiche les commentaires d'un post
            foreach($this->stream->query("SELECT id_author FROM posts WHERE id_post=$id_post") as $data){
                $post_author_id =$data['id_author'];
            }
            foreach($this->stream->query("SELECT * FROM commentaire WHERE post_id=$id_post") as $data){
                $up = $data['up'];
                $author = $this->buildUser($data['author_id']);
                $post_author = $this->buildUser($post_author_id);
            echo'<div class="article_aside_content_comment" id="article_aside_content_comment_'.$data['comment_id'].'">
                    '.$data['content'].'
                    <br/>
                    <br/>
                    <hr/>
                    <form name="profil_link_'.$data['comment_id'].'" method="POST" style="display:inline;" action="profil.php">
                    <input name="profil_link" type="hidden" value="'.$author['username'].'"/>
                    Posté par <a href="javascript: document.profil_link_'.$data['comment_id'].'.submit();" id="comment_author">'.$author['username'].'</a> le '.$data['creation_date'].'.
                    </form>
                    <br/>
                    ';
                    if($this->checkLike($data['comment_id'], $user_id)){
                    //echo '<script  style="display:none;">function form_like_'.$rand.'(){document.like_'.$rand.'.submit();}</script><form name="like_'.$rand.'" method="POST" style="display: inline;" action="abonnements.php"><input name="up" type="hidden" value="'.$data['comment_id'].'"/><a href="javascript:form_like_'.$rand.'();">Ne plus aimer</a>  </form>';
                    echo '<span id="comment_'.$data['comment_id'].'"><a href="javascript:form_like(\''.$data['comment_id'].'\',\''.$post_author['username'].'\');"> Ne plus aimer</a> ';
                    }
                    else{
                    echo '<span id="comment_'.$data['comment_id'].'"><a href="javascript:form_like(\''.$data['comment_id'].'\',\''.$post_author['username'].'\');"> Aimer</a> ';
                    }
                    if($up>1){
                        echo ''.$up.' personnes aimes. ';
                    }
                    else{
                        echo ''.$up.' personne aime. ';
                    }
                    if($user_id==$data['author_id'] ||$user_id == $post_author_id){
                        echo '</span>
                            <a href="javascript:delcom(\''.$data['comment_id'].'\');"><img alt="Supprimer" src="trash.png" title="Supprimer"/></a>';
                    }
            echo'</div>';
            }
        }
        public function createPost($image_url,$description,$title, $user_id, $privacy){ //Création d'un post
            Try{
                $description = addslashes($description);
                $title = addslashes($title);
                $rand = $rand = sha1(rand(0,9999999999999999));
                
                $description = addslashes($description);
                $title = addslashes($title);
                $description = htmlspecialchars($description);
                $title = htmlspecialchars($title);
                
                $date = date('Y-m-d H:i:s');
                $this->stream->exec("INSERT INTO `objectsIdCreator` VALUES(1,Null, '$rand')");
                foreach($this->stream->query("SELECT object_id FROM objectsIdCreator WHERE temp_id='$rand'") as $data){
                    $object_id = $data['object_id'];
                }
                $this->stream->exec("UPDATE objectsIdCreator SET temp_id = 0 WHERE temp_id = '$rand'");
                $this->stream->exec('INSERT INTO `daily_top` VALUES("'.$object_id.'","0")');
                $this->stream->exec('INSERT INTO `posts` VALUES("'.$object_id.'","'.$user_id.'","'.$title.'","'.$description.'","'.$image_url.'","0","'.$privacy.'","'.$date.'")');
            }catch(PDOException $e){
            }
        }
        public function displayPosts($user_id, $search=null, $profil_permission = null,$profil_visitor_id=null,$offset = 0){
            if($search==null && $profil_permission == null){
                $request = "SELECT * FROM posts WHERE private=0 AND NOT id_author = $user_id ORDER BY creation_date DESC  LIMIT 5 OFFSET $offset";
            }
            else if($search!=null && $profil_permission == null){
                $request = "SELECT * FROM posts WHERE private=0 AND titre like '%$search%' AND NOT id_author =$user_id ORDER BY creation_date DESC LIMIT 5 OFFSET $offset";
            }
            else if($search==null && $profil_permission == 1){// $profil_permission==1 correspond à visiteur sur la page profil
                $request = "SELECT * FROM posts WHERE private=0 AND id_author =$user_id ORDER BY creation_date DESC LIMIT 5 OFFSET $offset";
            }
            else if($search==null && $profil_permission == 2){// $profil_permission==2 correspond à propriétaire sur la page profil
                $request = "SELECT * FROM posts WHERE id_author =$user_id ORDER BY creation_date DESC LIMIT 5 OFFSET $offset";
            }
            else if($search!=null && $profil_permission == 1){// $profil_permission==1 correspond à visiteur sur la page profil
                $request = "SELECT * FROM posts WHERE private=0 AND titre like '%$search%' AND id_author =$user_id ORDER BY creation_date DESC LIMIT 5 OFFSET $offset";
            }
            else if($search!=null && $profil_permission == 2){// $profil_permission==2 correspond à propriétaire sur la page profil
                $request = "SELECT * FROM posts WHERE id_author =$user_id AND titre like '%$search%' ORDER BY creation_date DESC LIMIT 5 OFFSET $offset";
            }
            try{
                
                if($this->stream->query($request)){
                
                    foreach($this->stream->query($request) as $data){
                    
                        $rand = sha1(rand(0,9999999999999999));
                        $author = $this->buildUser($data['id_author']);
        echo '<article class="posts_article">';
                if($profil_permission != 1 && $profil_permission != 2){
                echo '<form name="profil_link_'.$data['id_post'].'" method="POST" style="display:inline;" action="profil.php">
                        <input name="profil_link" type="hidden" value="'.$author['username'].'"/><h3>
						<a href="javascript: document.profil_link_'.$data['id_post'].'.submit();" class="post_author">'.$author['username'].'</a></h3>
						</form>
						';
                        if(!$this->checkFollowed($user_id,$data['id_author'])){
                        echo '
                            <h3>(<a class="post_follow" href="javascript: follow(\''.$author["username"].'\')">Suivre</a>)</h3>';
                        }
                        else{
                        echo '
                            <h3>(<a class="post_follow" href="javascript: stop_follow(\''.$author['username'].'\')">Ne plus suivre</a>)</h3>';
                        }
                    echo '
                    <br/>
                    <br/>
                    <h3>
                    '.$data['titre'].'
                    <br/>
					</h3>';
					}
					else if($profil_permission==1){
					
					echo '<h3>
                        '.$data['titre'].'
                        <br/></h3>';
					}
					else if($profil_permission==2){
                        
                        if(!$this->getPostStatus($data['id_post'])){
                            $state_moduler = "privée";
                            $private = "publique";
                        }else{
                            $state_moduler = "publique";
                            $private = "privée";
                        }
					echo '<button style="color: #337ab7;margin:0;padding:0;background-color: Transparent;border: none;cursor:pointer;outline:none;" type="button" onclick="document.getElementById(\'modify_post_button_'.$data['id_post'].'\').classList.add(\'hide\');document.getElementById(\'modify_post_status_'.$data['id_post'].'\').classList.remove(\'hide\');document.getElementById(\'validate_modify_post_button_'.$data['id_post'].'\').classList.remove(\'hide\');document.getElementById(\'post_title'.$data['id_post'].'\').classList.add(\'hide\');document.getElementById(\'validate_modify_post_title_'.$data['id_post'].'\').classList.remove(\'hide\');document.getElementById(\'post_description'.$data['id_post'].'\').classList.add(\'hide\');document.getElementById(\'validate_post_description_'.$data['id_post'].'\').classList.remove(\'hide\');" id="modify_post_button_'.$data['id_post'].'"><h3>Modifier Post</h3></button>
                            <button class="hide" style="color: #337ab7;margin:0;padding:0;background-color: Transparent;border: none;cursor:pointer;outline:none;" type="button" onclick="document.getElementById(\'modify_post_button_'.$data['id_post'].'\').classList.remove(\'hide\');document.getElementById(\'modify_post_status_'.$data['id_post'].'\').classList.add(\'hide\');document.getElementById(\'validate_modify_post_button_'.$data['id_post'].'\').classList.add(\'hide\');document.getElementById(\'post_title'.$data['id_post'].'\').classList.remove(\'hide\');document.getElementById(\'validate_modify_post_title_'.$data['id_post'].'\').classList.add(\'hide\');document.getElementById(\'post_description'.$data['id_post'].'\').classList.remove(\'hide\');document.getElementById(\'validate_post_description_'.$data['id_post'].'\').classList.add(\'hide\');validate_modifications(\''.$data['id_post'].'\')" id="validate_modify_post_button_'.$data['id_post'].'"><h3>Valider modifications</h3></button>
                            <h3> - </h3>
                        <a href="javascript:delpost(\''.$data['id_post'].'\');"><h3>Supprimer</h3></a></form>
                        <br/>
                        <h3>
                        <br/>
                        <span id="post_title'.$data['id_post'].'">'.$data['titre'].'</span>
                        <span class="hide" id="validate_modify_post_title_'.$data['id_post'].'"><input id="modify_post_title_'.$data['id_post'].'" type="text" value="'.$data['titre'].'"/></span>
                        </h3>
                        <div style="display:inline;" id="modify_post_status_'.$data['id_post'].'" class="hide">
                            <h3> (Statut: '.$private.')</h3>
                            <input id="post_private_'.$data['id_post'].'" type="hidden" value="'.$private.'"/>
                            
                            <h3><a id="state_moduler_1_'.$data['id_post'].'" href="javascript: document.getElementById(\'state_moduler_1_'.$data['id_post'].'\').classList.add(\'hide\');document.getElementById(\'state_moduler_2_'.$data['id_post'].'\').classList.remove(\'hide\');document.getElementById(\'post_private_'.$data['id_post'].'\').setAttribute(\'value\',\''.$state_moduler.'\');" class="post_follow">Prochain statut: '.$private.'</a></h3>
                            
                            <h3><a class="hide" id="state_moduler_2_'.$data['id_post'].'" href="javascript: document.getElementById(\'state_moduler_1_'.$data['id_post'].'\').classList.remove(\'hide\');document.getElementById(\'state_moduler_2_'.$data['id_post'].'\').classList.add(\'hide\');document.getElementById(\'post_private_'.$data['id_post'].'\').setAttribute(\'value\',\''.$private.'\');" class="post_follow">Prochain statut: '.$state_moduler.'</a></h3>
                        </div>
                        ';
					}
                echo '<br/>
                    <br/>
					<div id="like_dislike_'.$data['id_post'].'">';
					
					if($profil_visitor_id==null){
                        if(!$this->checkLike($data['id_post'], $user_id)){
                        echo '<div style="position:absolute;z-index:2">
                        <input alt="Dislike" type="image" src="dislike.png" class="hide" onmouseout="document.getElementById(\'dislike_'.$data['id_post'].'\').classList.add(\'hide\')" onmouseover="document.getElementById(\'dislike_'.$data['id_post'].'\').classList.remove(\'hide\')" id="dislike_'.$data['id_post'].'" onclick="javascript:form_like(\''.$data['id_post'].'\',\''.$author['username'].'\');" title="Aimer"/></div>
                            <div style="position:relative;z-index:1"><img alt="PostImage" class="post_img" onmouseout="document.getElementById(\'dislike_'.$data['id_post'].'\').classList.add(\'hide\')" onmouseover="document.getElementById(\'dislike_'.$data['id_post'].'\').classList.remove(\'hide\')" src="'.$data['image'].'"/></div>';
                        }
                        else{
                        echo '<div style="position:absolute;z-index:2">
                        <input alt="Like" type="image" id="like_'.$data['id_post'].'" src="like.png" class="hide" onmouseout="document.getElementById(\'like_'.$data['id_post'].'\').classList.add(\'hide\')" onmouseover="document.getElementById(\'like_'.$data['id_post'].'\').classList.remove(\'hide\')" onclick="javascript:form_like(\''.$data['id_post'].'\',\''.$author['username'].'\');" title="Ne plus aimer"/></div>
                            <div style="position:relative;z-index:1"><img alt="PostImage" class="post_img" onmouseout="document.getElementById(\'like_'.$data['id_post'].'\').classList.add(\'hide\')" onmouseover="document.getElementById(\'like_'.$data['id_post'].'\').classList.remove(\'hide\')" src="'.$data['image'].'"/></div>';
                        }
                    }else{
                        if(!$this->checkLike($data['id_post'], $profil_visitor_id)){
                        echo '<div style="position:absolute;z-index:2">
                        <input alt="Dislike" type="image" src="dislike.png" class="hide" onmouseout="document.getElementById(\'dislike_'.$data['id_post'].'\').classList.add(\'hide\')" onmouseover="document.getElementById(\'dislike_'.$data['id_post'].'\').classList.remove(\'hide\')" id="dislike_'.$data['id_post'].'" onclick="javascript:form_like(\''.$data['id_post'].'\',\''.$author['username'].'\');" title="Aimer"/></div>
                            <div style="position:relative;z-index:1"><img alt="PostImage" class="post_img" onmouseout="document.getElementById(\'dislike_'.$data['id_post'].'\').classList.add(\'hide\')" onmouseover="document.getElementById(\'dislike_'.$data['id_post'].'\').classList.remove(\'hide\')" src="'.$data['image'].'"/></div>';
                        }
                        else{
                        echo '<div style="position:absolute;z-index:2">
                        <input alt="Like" type="image" id="like_'.$data['id_post'].'" src="like.png" class="hide" onmouseout="document.getElementById(\'like_'.$data['id_post'].'\').classList.add(\'hide\')" onmouseover="document.getElementById(\'like_'.$data['id_post'].'\').classList.remove(\'hide\')" onclick="javascript:form_like(\''.$data['id_post'].'\',\''.$author['username'].'\');" title="Ne plus aimer"/></div>
                            <div style="position:relative;z-index:1"><img alt="PostImage" class="post_img" onmouseout="document.getElementById(\'like_'.$data['id_post'].'\').classList.add(\'hide\')" onmouseover="document.getElementById(\'like_'.$data['id_post'].'\').classList.remove(\'hide\')" src="'.$data['image'].'"/></div>';
                        }
                    }
                echo '</div>
					<div id="article_aside_content_'.$data['id_post'].'" class = "article_aside_content">
						<table class="article_aside_content_link_container">
							<tr>
								<th class="article_aside_content_link_description_container">
									<button type="button" onclick="document.getElementById(\'article_aside_content_comments_'.$data['id_post'].'\').classList.add(\'hide\');document.getElementById(\'article_aside_content_description_'.$data['id_post'].'\').classList.remove(\'hide\');document.getElementById(\'article_aside_content_'.$data['id_post'].'\').style.backgroundColor  = \'#447788\';" class="article_aside_content_link_description">Description</button>
								</th>
								<th class="article_aside_content_link_comments_container">
									<button type="button" onclick="document.getElementById(\'article_aside_content_description_'.$data['id_post'].'\').classList.add(\'hide\');document.getElementById(\'article_aside_content_comments_'.$data['id_post'].'\').classList.remove(\'hide\');document.getElementById(\'article_aside_content_'.$data['id_post'].'\').style.backgroundColor  = \'#004789\';" class="article_aside_content_link_comments">Commentaires</button>
								</th>
							</tr>
						</table>
						<div class="article_aside_content_container">
							<div id = "article_aside_content_description_'.$data['id_post'].'" class="article_aside_content_description"><span id="post_description'.$data['id_post'].'">'.$data['description'].'</span>
                        <span class="hide" id="validate_post_description_'.$data['id_post'].'"><textarea style="width:100%;height:100%;" id="modify_post_description_'.$data['id_post'].'" autocomplete="off" maxlength="200" minlength="5">'.$data['description'].'</textarea></span>

								<br/>
								<hr/>
								<br/>
								Image postée le '.$data['creation_date'].'.
							</div>
							
							<div id = "article_aside_content_comments_'.$data['id_post'].'" class="hide article_aside_content_comments">';
							if($profil_visitor_id==null){
                                $this->displayComments($data['id_post'], $user_id);
                                }
                            else{
                                $this->displayComments($data['id_post'], $profil_visitor_id);
                            }
                            echo '<div class="article_aside_content_comment">
										<textarea id="comment_content_'.$data['id_post'].'" class="article_aside_comment" autocomplete="off" maxlength="200" minlength="5" placeholder="Ecrire un commentaire..." rows="3"></textarea>
										<hr/>
										<button onclick="addcom(\''.$data['id_post'].'\')">Envoyer</button>
								</div>
								<br/>
								<hr/>
								<br/>
								Image postée le '.$data['creation_date'].'.
							</div>
						</div>
					</div>
				</article>';
                    }
                }
            }catch(PDOException $e){
            }
        }
    }
?>
