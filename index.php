<?php
session_start();
$_SESSION['user_id'] = 0;

function chargerClasse($classe) {
    require $classe . '.php';
}

spl_autoload_register('chargerClasse');
chargerClasse('BddManager');
$dataBase = new BddManager('localhost', 'chris', 'PASSWORD', "paint'isen");
if (isset($_COOKIE['id_cookie'])) {
    $_SESSION['user_id'] = $dataBase->checkCookie($_COOKIE['id_cookie'], $_SERVER['REMOTE_ADDR']);
    if ($_SESSION['user_id'] != 0) {
        header("Location: home.php");
    }
}

if (isset($_POST['registration'])) {
    $message = $dataBase->addUser($_POST['login'], $_POST['password'], $_POST['username']);
} else if (isset($_POST['connection'])) {
    $_SESSION['user_id'] = $dataBase->connectUser($_POST['login'], $_POST['password']);
    if ($_SESSION['user_id'] != 0) {
        $dataBase->putCookie($_SESSION['user_id'], $_SERVER['REMOTE_ADDR']);
        header("Location: home.php");
    }
}
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8"/>
        <link rel="stylesheet" href="index.css">
        <title>Inscription</title>
    </head>
    <body>
        <video autoplay muted loop>
            <source src= "background_Paint'ISEN.mp4">
        </video>
        <img src="title.png" alt="title"/>
        <div id = "registration" <?php
        if (!isset($_POST['registration'])) {
            echo'class="hide"';
        }
        ?>>
            <form method="post" action="#">
                <table class="field">
                    <tr>
                        <th>
                            Entrer un nom de compte
                        </th>
                        <td>
                            <input name="login" type="text" minlength= "4" maxlength="65" required />
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Entrer un mot de passe
                        </th>
                        <td>
                            <input name="password" type="password" minlength= "5" maxlength="65" required />
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Entrer un nom d'utilisateur
                        </th>
                        <td>
                            <input name="username" type="text" pattern="[a-zA-Z][a-zA-Z0-9\s]*" minlength= "2" maxlength="20" title="Seulement lettres et/ou chiffres (longueur:2-20)." autocomplete="off" required />
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td>
                            <input type="submit" value= "INSCRIPTION" name="registration"/>
                        </td>
                        <td>
                            <input type="button" value= "Retour" onclick="document.getElementById('choice').classList.remove('hide');document.getElementById('registration').classList.add('hide');"/>
                        </td>
                    </tr>
                </table>
                <table>
                    <?php
                    if (isset($message)) {
                        for ($i = 1; $i < count($message); $i++) {
                            echo"<tr>
                                        <td style='color:$message[0];'>$message[$i]</td>
                                </tr>";
                        }
                    }
                    ?>
                </table>
            </form>
        </div>
        <div id = "connection" <?php
        if (!isset($_POST['connection'])) {
            echo'class="hide"';
        }
        ?>>
            <form method="post" action="#">
                <table class="field">
                    <tr>
                        <th>
                            Nom de compte
                        </th>
                        <td>
                            <input name="login" type="text" minlength= "4" maxlength="65" required />
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Mot de passe
                        </th>
                        <td>
                            <input name="password" type="password" minlength= "5" maxlength="65" required />
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td>
                            <input type="submit" value= "Connexion" name="connection"/>
                        </td>
                        <td>
                            <input type="button" value= "Retour" onclick="document.getElementById('choice').classList.remove('hide');document.getElementById('connection').classList.add('hide');"/>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
        <div id = "choice" <?php
        if (isset($_POST['registration']) || isset($_POST['connection'])) {
            echo'class="hide"';
        }
        ?>>
            <table class="field">
                <tr>
                    <td>
                        <button type="button" class="button" id="synchronisation" name="login" onClick="document.getElementById('connection').classList.remove('hide');document.getElementById('choice').classList.add('hide');" >Connexion</button>
                    </td>
                    <td>
                        <button type="button" class="button" id="initialisation" name="login" onClick="document.getElementById('registration').classList.remove('hide');document.getElementById('choice').classList.add('hide');" >Inscription</button>
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>
