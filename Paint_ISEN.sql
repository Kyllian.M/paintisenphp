-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Mer 07 Novembre 2018 à 16:07
-- Version du serveur :  5.7.24-0ubuntu0.18.04.1
-- Version de PHP :  7.2.10-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `Paint'ISEN`
--

-- --------------------------------------------------------

--
-- Structure de la table `abonnements`
--

CREATE TABLE `abonnements` (
  `follower` bigint(20) NOT NULL,
  `followed` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `abonnements`
--

INSERT INTO `abonnements` (`follower`, `followed`) VALUES
(30, 29),
(35, 29),
(35, 27),
(27, 35),
(27, 29);

-- --------------------------------------------------------

--
-- Structure de la table `authentification`
--

CREATE TABLE `authentification` (
  `user_login` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `authentification`
--

INSERT INTO `authentification` (`user_login`, `user_password`, `user_id`) VALUES
('krostof', '7c4a8d09ca3762af61e59520943dc26494f8941b', 27),
('dzadzadaz', '4a6aee201e8f63ac90034e0ab5d1b35d39921e62', 28),
('FMCOfficiel', '33f6d6d4aea5a65103a9ebca1ca6a0f765492739', 29),
('Sebastien', '7c4a8d09ca3762af61e59520943dc26494f8941b', 30),
('slaut', '1bfbdf35b1359fc6b6f93893874cf23a50293de5', 31),
('zfeazf', '1434a28f7dcee7c3827be647779784ec0733cc5d', 32),
('chris', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 33),
('admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 34),
('lorenzo', '86995a091f8a1385082bd10715048b52ee386a6a', 35),
('volto', '7c4a8d09ca3762af61e59520943dc26494f8941b', 36);

-- --------------------------------------------------------

--
-- Structure de la table `commentaire`
--

CREATE TABLE `commentaire` (
  `author_id` bigint(20) NOT NULL,
  `content` text CHARACTER SET latin1 NOT NULL,
  `creation_date` datetime NOT NULL,
  `up` int(11) NOT NULL,
  `comment_id` bigint(20) NOT NULL,
  `post_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `commentaire`
--

INSERT INTO `commentaire` (`author_id`, `content`, `creation_date`, `up`, `comment_id`, `post_id`) VALUES
(35, 'xqwscscsqscq', '2018-10-31 22:00:19', 1, 8, 1),
(35, 'al;oua', '2018-11-01 21:07:03', 0, 13, 1),
(27, 'gerhrehtrhtr', '2018-11-03 01:09:27', 0, 15, 1),
(27, 'azdzadaz', '2018-11-03 01:43:00', 1, 27, 25),
(27, 'zadazdaz', '2018-11-03 02:47:51', 1, 28, 25),
(27, 'efezfzefez', '2018-11-03 03:23:07', 1, 30, 25),
(27, 'gregreger', '2018-11-03 03:24:09', 1, 31, 25),
(27, 'nhbhhbjuh', '2018-11-03 03:24:25', 1, 32, 24),
(27, 'dazdazdaz', '2018-11-03 15:25:18', 1, 35, 20),
(35, 'INSERT INTO', '2018-11-06 10:34:40', 0, 41, 25),
(27, 'gegege', '2018-11-07 14:36:58', 0, 46, 23),
(27, 'arg8', '2018-11-07 14:56:34', 1, 58, 36),
(35, 'ad', '2018-11-07 15:40:30', 1, 71, 36),
(35, 'bonsoir', '2018-11-07 15:42:24', 1, 72, 36),
(35, 'haha', '2018-11-07 15:42:33', 0, 73, 36),
(35, 'dada', '2018-11-07 15:43:10', 0, 74, 36),
(35, 'gaga', '2018-11-07 15:43:59', 0, 75, 36),
(35, 'dada', '2018-11-07 15:44:03', 1, 76, 20),
(35, 'profff', '2018-11-07 15:52:07', 0, 77, 20),
(35, 'ad', '2018-11-07 15:57:05', 0, 78, 20);

-- --------------------------------------------------------

--
-- Structure de la table `cookies`
--

CREATE TABLE `cookies` (
  `id_cookie` varchar(255) NOT NULL,
  `user_ip` varchar(15) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `timetolive` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `cookies`
--

INSERT INTO `cookies` (`id_cookie`, `user_ip`, `user_id`, `timetolive`) VALUES
('40d1e95b520841b038878229101fa662c5b5ee19', '83.113.216.109', 35, 1544194959);

-- --------------------------------------------------------

--
-- Structure de la table `daily_top`
--

CREATE TABLE `daily_top` (
  `object_id` bigint(20) NOT NULL,
  `up` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `daily_top`
--

INSERT INTO `daily_top` (`object_id`, `up`) VALUES
(1, 0),
(16, 0),
(17, 0),
(18, 1),
(19, 0),
(20, 0),
(21, 0),
(22, 0),
(23, 0),
(24, 1),
(25, 1),
(36, 2);

-- --------------------------------------------------------

--
-- Structure de la table `objectsIdCreator`
--

CREATE TABLE `objectsIdCreator` (
  `type` tinyint(4) NOT NULL,
  `object_id` bigint(20) UNSIGNED NOT NULL,
  `temp_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `objectsIdCreator`
--

INSERT INTO `objectsIdCreator` (`type`, `object_id`, `temp_id`) VALUES
(1, 1, '0'),
(0, 2, '0'),
(0, 3, '0'),
(0, 4, '0'),
(0, 5, '0'),
(0, 6, '0'),
(0, 7, '0'),
(0, 8, '0'),
(0, 9, '0'),
(0, 10, '0'),
(0, 11, '0'),
(0, 12, '0'),
(0, 13, '0'),
(0, 14, '0'),
(0, 15, '0'),
(1, 16, '0'),
(1, 17, '0'),
(1, 18, '0'),
(1, 19, '0'),
(1, 20, '0'),
(1, 21, '0'),
(1, 22, '0'),
(1, 23, '0'),
(1, 24, '0'),
(1, 25, '0'),
(0, 26, '0'),
(0, 27, '0'),
(0, 28, '0'),
(0, 29, '0'),
(0, 30, '0'),
(0, 31, '0'),
(0, 32, '0'),
(0, 33, '0'),
(0, 34, '0'),
(0, 35, '0'),
(1, 36, '0'),
(0, 37, '0'),
(0, 38, '0'),
(0, 39, '0'),
(0, 40, '0'),
(0, 41, '0'),
(0, 42, '0'),
(0, 43, '0'),
(0, 44, '0'),
(0, 45, '0'),
(0, 46, '0'),
(0, 47, '0'),
(0, 48, '0'),
(0, 49, '0'),
(0, 50, '0'),
(0, 51, '0'),
(0, 52, '0'),
(0, 53, '0'),
(0, 54, '0'),
(0, 55, '0'),
(0, 56, '0'),
(0, 57, '0'),
(0, 58, '0'),
(0, 59, '0'),
(0, 60, '0'),
(0, 61, '0'),
(0, 62, '0'),
(0, 63, '0'),
(0, 64, '0'),
(0, 65, '0'),
(0, 66, '0'),
(0, 67, '0'),
(0, 68, '0'),
(0, 69, '0'),
(0, 70, '0'),
(0, 71, '0'),
(0, 72, '0'),
(0, 73, '0'),
(0, 74, '0'),
(0, 75, '0'),
(0, 76, '0'),
(0, 77, '0'),
(0, 78, '0'),
(0, 79, '0');

-- --------------------------------------------------------

--
-- Structure de la table `posts`
--

CREATE TABLE `posts` (
  `id_post` bigint(20) NOT NULL,
  `id_author` bigint(20) NOT NULL,
  `description` text NOT NULL,
  `titre` varchar(30) NOT NULL,
  `image` varchar(255) NOT NULL,
  `up` int(11) NOT NULL,
  `private` int(11) NOT NULL,
  `creation_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `posts`
--

INSERT INTO `posts` (`id_post`, `id_author`, `description`, `titre`, `image`, `up`, `private`, `creation_date`) VALUES
(1, 27, 'FMC power ?. On ne sait toujours pas.', 'titre lambda2', 'https://cdn.pixabay.com/photo/2018/10/18/19/02/woman-3757184_960_720.jpg', 0, 1, '2018-10-31 20:42:03'),
(17, 27, 'FMC power ?. On ne sait toujours pas.', 'titre lambda2', 'https://cdn.pixabay.com/photo/2018/10/18/19/02/woman-3757184_960_720.jpg', 0, 0, '2018-11-03 01:11:56'),
(19, 27, 'FMC power ?. On ne sait toujours pas.', 'titre lambda2', 'https://cdn.pixabay.com/photo/2018/10/18/19/02/woman-3757184_960_720.jpg', 0, 1, '2018-11-03 01:11:58'),
(20, 27, 'FMC power ?. On ne sait toujours pas.', 'titre lambda2', 'https://cdn.pixabay.com/photo/2018/10/18/19/02/woman-3757184_960_720.jpg', 0, 0, '2018-11-03 01:11:59'),
(21, 35, 'FMC power ?. On ne sait toujours pas.', 'titre lambda2', 'https://cdn.pixabay.com/photo/2018/10/18/19/02/woman-3757184_960_720.jpg', 0, 0, '2018-11-03 01:12:08'),
(22, 35, 'FMC power ?. On ne sait toujours pas.', 'titre lambda2', 'https://cdn.pixabay.com/photo/2018/10/18/19/02/woman-3757184_960_720.jpg', 0, 0, '2018-11-03 01:12:09'),
(23, 35, 'FMC power ?. On ne sait toujours pas.', 'titre lambda2', 'https://cdn.pixabay.com/photo/2018/10/18/19/02/woman-3757184_960_720.jpg', 0, 0, '2018-11-03 01:12:11'),
(24, 35, 'FMC power ?. On ne sait toujours pas.', 'titre lambda2', 'https://cdn.pixabay.com/photo/2018/10/18/19/02/woman-3757184_960_720.jpg', 1, 0, '2018-11-03 01:12:12'),
(25, 35, 'ferferferfer', 'titr\'e ðŸ¤¬ ', 'https://cdn.pixabay.com/photo/2018/10/18/19/02/woman-3757184_960_720.jpg', 1, 0, '2018-11-03 01:12:13'),
(36, 29, 'FMC power ?. On ne sait toujours pas.', 'titre lambda2', 'https://images.emojiterra.com/google/android-pie/share/1f937-2642.jpg', 2, 0, '2018-11-03 17:01:01');

-- --------------------------------------------------------

--
-- Structure de la table `profils`
--

CREATE TABLE `profils` (
  `username` varchar(65) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `profil_image` varchar(255) DEFAULT 'sub_ico.png',
  `user_permissions` int(11) DEFAULT '1',
  `creation_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `profils`
--

INSERT INTO `profils` (`username`, `user_id`, `profil_image`, `user_permissions`, `creation_date`) VALUES
('christophevolto', 27, 'https://padelmagazine.fr/wp-content/uploads/2018/09/photo-1456983933114-c22026990f4b-3.jpeg', 1, '2018-10-27 21:48:44'),
('christophe volto', 28, 'sub_ico.png', 1, '2018-10-27 21:48:44'),
('FMCOfficiel', 29, '29.png', 1, '2018-10-27 21:48:44'),
('Sebastien', 30, 'https://cdn.pixabay.com/photo/2017/06/10/22/58/composing-2391033_960_720.jpg', 1, '2018-10-27 21:48:44'),
('Jean', 31, 'sub_ico.png', 1, '2018-10-27 21:48:44'),
('Kevin', 32, 'sub_ico.png', 1, '2018-10-27 21:48:44'),
('chris', 33, 'sub_ico.png', 1, '2018-10-27 21:48:44'),
('admin', 34, 'sub_ico.png', 1, '2018-10-28 02:17:59'),
('lorenzo', 35, '35.png', 1, '2018-10-28 02:31:08'),
('chris volto', 36, 'sub_ico.png', 1, '2018-10-29 19:46:18');

-- --------------------------------------------------------

--
-- Structure de la table `ups`
--

CREATE TABLE `ups` (
  `object_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `ups`
--

INSERT INTO `ups` (`object_id`, `user_id`) VALUES
(2, 35),
(4, 35),
(8, 35),
(26, 27),
(28, 27),
(25, 27),
(27, 27),
(30, 27),
(31, 27),
(32, 27),
(24, 27),
(34, 27),
(18, 27),
(35, 27),
(36, 35),
(37, 35),
(38, 27),
(40, 27),
(36, 27),
(43, 27),
(42, 27),
(58, 27),
(59, 27),
(76, 35),
(71, 35),
(72, 35),
(79, 35);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `authentification`
--
ALTER TABLE `authentification`
  ADD PRIMARY KEY (`user_id`);

--
-- Index pour la table `commentaire`
--
ALTER TABLE `commentaire`
  ADD PRIMARY KEY (`comment_id`);

--
-- Index pour la table `objectsIdCreator`
--
ALTER TABLE `objectsIdCreator`
  ADD PRIMARY KEY (`object_id`);

--
-- Index pour la table `profils`
--
ALTER TABLE `profils`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `authentification`
--
ALTER TABLE `authentification`
  MODIFY `user_id` bigint(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT pour la table `objectsIdCreator`
--
ALTER TABLE `objectsIdCreator`
  MODIFY `object_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;
DELIMITER $$
--
-- Événements
--
CREATE DEFINER=`chris`@`localhost` EVENT `daily_top` ON SCHEDULE EVERY 1 DAY STARTS '2018-10-28 00:00:00' ON COMPLETION NOT PRESERVE ENABLE COMMENT 'Pensez à faire SET GLOBAL event_scheduler = ON;' DO UPDATE `daily_top` SET `up`=0 WHERE 1$$

DELIMITER ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
