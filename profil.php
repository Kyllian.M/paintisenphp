<?php
    session_start();
    if($_SESSION['user_id'] !=0){
    function chargerClasse($classe){
        require $classe.'.php';
	}
	spl_autoload_register('chargerClasse');
	chargerClasse('BddManager');
	chargerClasse('User');
	$dataBase = new BddManager('localhost','chris','PASSWORD',"Paint'ISEN");
	$user = new User($dataBase->buildUser($_SESSION['user_id']));
    $_SESSION['profil_link'] = $user->getUsername();
	$profil = new User($dataBase->buildUser("",$_SESSION['profil_link']));
	
    if(isset($_POST['profil_link'])){
        $_SESSION['profil_link'] = htmlspecialchars($_POST['profil_link']);
        $profil = new User($dataBase->buildUser("",$_SESSION['profil_link']));
    }
    if(isset($_POST['comment_content'])){
        $comment_content = htmlspecialchars($_POST['comment_content']);
        $id_post = htmlspecialchars($_POST['id_post']);
        $dataBase->createComment($user->getUser_id(),$id_post,$comment_content);
    }
    if(isset($_POST['profil_image_url'])){
        $user->setProfil_image(htmlspecialchars($_POST['profil_image_url']));
        $dataBase->updateUser($user);
    }
    if(isset($_POST['changepass'])){
        if(strlen(htmlspecialchars($_POST['changepass'])) >=5 && strlen(htmlspecialchars($_POST['changepass'])) <=65){
            $dataBase->changeUserPass(htmlspecialchars($_POST['changepass']),$_SESSION['user_id']);
        }
    }
    if(isset($_FILES['profil_image'])){
        $errors= array();
        $file_name = $_FILES['profil_image']['name'];
        $file_size =$_FILES['profil_image']['size'];
        $file_tmp =$_FILES['profil_image']['tmp_name'];
        $file_type=$_FILES['profil_image']['type'];
        $file_name_Exploded = explode('.',$file_name);
        $image_ext = strtolower(end($file_name_Exploded));
        $file_ext=strtolower(end($file_name_Exploded));

        $expensions= array("jpeg","jpg","png", "gif");

        if(in_array($file_ext,$expensions)=== false){
           $errors[]="Extension non supportée, veuillez choisir les format JPEG ou PNG.";
        }

        if($file_size > 2097152){
           $errors[]='Le fichier ne doit excéder  2 MO';
        }

        if(empty($errors)==true){
           move_uploaded_file($file_tmp,$_SESSION['user_id'].'.'.$file_ext);
           $user->setProfil_image($_SESSION['user_id'].'.'.$file_ext);
           $dataBase->updateUser($user);
        }else{
           print_r($errors);
        }
        header("Location:profil.php");
   }
   if(isset($_POST['post_modify_id']) && isset($_POST['post_modify_description']) && isset($_POST['post_modify_title']) && isset($_POST['post_modify_private'])){
            $dataBase->updatePost($_POST['post_modify_id'], $_POST['post_modify_description'],$_POST['post_modify_title'], $_POST['post_modify_private']);
        }
    $fp = fopen("profilResponse.html", 'r');
    if ($fp) {
        $strHTML = explode("<?php INSERT HERE ?>", fread($fp, filesize("profilResponse.html")));
    }
    fclose($fp); 
    echo $strHTML[0];
    echo $profil->getUsername();
    echo $strHTML[1];
    $rand = sha1(rand(0,9999999999999999));
    echo $rand;
    echo $strHTML[2];
    echo $rand;
    echo $strHTML[3];
    echo $rand;
    echo $strHTML[4];
    echo $user->getUsername();
    echo $strHTML[5];
    echo $rand;
    echo $strHTML[6];
    $rand = sha1(rand(0,9999999999999999));
    echo $rand;
    echo $strHTML[7];
    echo $rand;
    echo $strHTML[8];
    echo $rand;
    echo $strHTML[9];
    echo $user->getUsername();
    echo $strHTML[10];
    echo $rand;
    echo $strHTML[11];
    $dataBase->displayFollowers($user->getUser_id());
    echo $strHTML[12];
    echo $profil->getUsername();
    echo $strHTML[13];
    echo $profil->getUsername();
    echo $strHTML[14];
    echo $profil->getProfil_image();
    echo $strHTML[15];
    if($user->getUser_id()!=$profil->getUser_id()){
        if(!$dataBase->checkFollowed($user->getUser_id(),$profil->getUser_id())){
            echo '
                (<a href="javascript: follow(\''.$profil->getUsername().'\')">Suivre</a>)';
            }
        else{
            echo '
            (<a href="javascript: stop_follow(\''.$profil->getUsername().'\')">Ne plus suivre</a>)';
        }
    }
    echo $strHTML[16];
    if($user->getUser_id()==$profil->getUser_id()){
        echo"<section>
                <h2 id=\"posts_title\">
                    Section personnelle
                </h2>
                <table style=\"height:300px;margin:5% 5% 5% 0;text-align:center;padding:5% 5% 5% 0;width:100%;\">
                    <tr>
                        <td>
                            <h3>
                                Modifier image de profil:
                            </h3>
                        </td>
                        <td>
                            <form ENCTYPE=\"multipart/form-data\" method=\"POST\" action=\"\">
                            <input name=\"profil_image\" style=\"display:inline;\" type='file' onchange=\"readURL(this);\" />
                            <img style='width:100px;border-radius:50%;height:100px;border:solid black 1px;' id=\"blah\" src=\"http://placehold.it/100\" alt=\"your image\" /> <button>Envoyer</button>
                            </form >ou <form method=\"POST\" action=\"\"><input name=\"profil_image_url\" type=\"url\" placeholder=\"Paint'ISEN.fr/photo.jpg\"> <button>Envoyer</button></form>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h3>
                                Modifier mot de passe:
                            </h3>
                        </td>
                        <td>
                            <form method=\"POST\" action=\"\">
                                 <input name=\"changepass\" type=\"password\" minlength= \"5\" maxlength=\"65\" required />
                                <input type=\"submit\" value=\"Valider\"/>
                            </form>
                        </td>
                    </tr>
                </table>
                <br/>
                <br/>
            </section>";
    }
    echo $strHTML[17];
    if($user->getUser_id()!=$profil->getUser_id()){
        echo 'Les publications de '.$profil->getUsername().'';
    }else{
        echo 'Mes publications';
    }
    echo $strHTML[18];
    if ($user->getUser_id() == $profil->getUser_id()) {
        $profil_permission = 2;
    } else {
        $profil_permission = 1;
    }
    if (isset($_POST['searchbar'])) {
        $dataBase->displayPosts($profil->getUser_id(), htmlspecialchars($_POST['searchbar']), $profil_permission, $user->getUser_id());
        echo "<script>var imgSearch = " . htmlspecialchars($_POST['searchbar']) . ";</script>";
    } else {
        $dataBase->displayPosts($profil->getUser_id(), null, $profil_permission, $user->getUser_id());
        echo "<script>var imgSearch = null;</script>";
    }
    echo $strHTML[19];
    echo $profil->getUser_id();
    echo $strHTML[20];
    echo $profil_permission;
    echo $strHTML[21];
    echo $user->getUser_id();
    echo $strHTML[22];
    echo $profil->getUser_id();
    echo $strHTML[23];
    echo $profil_permission;
    echo $strHTML[24];
    echo $user->getUser_id();
    echo $strHTML[25];
} else {
    header('Location: index.php');
}
?>
