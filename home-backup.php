<?php
    session_start();
    if($_SESSION['user_id'] !=0){
    function chargerClasse($classe){
        require $classe.'.php';
	}
	spl_autoload_register('chargerClasse');
	chargerClasse('BddManager');
	chargerClasse('User');
    
    $dataBase = new BddManager('localhost','chris','53132834353',"Paint'ISEN");
    $dataBase->putCookie($_SESSION['user_id'],$_SERVER['REMOTE_ADDR']);
    
    $user = new User($dataBase->buildUser($_SESSION['user_id']));
    
/*$dataBase->createPost("https://cdn.pixabay.com/photo/2017/05/21/15/14/balloon-2331488_960_720.jpg","tour en montgolfière","
Image type	JPG<br/>
Résolution	3000×1876
<br/>
Date de téléchargement	23 mai 2017
<br/>
Affichages	167366
<br/>
Téléchargements	103093
", $user->getUser_id(), "0");*/
    
    if(isset($_POST['submit_comment']) && isset($_POST['comment_content'])){
        $comment_content = htmlspecialchars($_POST['comment_content']);
        $id_post = htmlspecialchars($_POST['id_post']);
        $dataBase->createComment($user->getUser_id(),$id_post,$comment_content);
    }
    
echo "<!DOCTYPE html>
<html lang=\"fr\">
	<head>
		<meta charset=utf-8 />
		<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
		<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">
		<link rel=\"stylesheet\" href=\"style.css\"/>
		<link rel=\"stylesheet\" href=\"article_aside_content_max.css\"/>
		<link rel=\"stylesheet\" href=\"article_aside_content_min.css\"/>
		<link rel=\"stylesheet\" href=\"article_aside_content_comments.css\"/>
		<link rel=\"stylesheet\" href=\"article_aside_content_description.css\"/>
		<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>
		<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>
		<script>
			$(document).ready(function(){
				$('[data-toggle=\"tooltip\"]').tooltip();   
			});
			function hideIcon(self) {
				if(self.value != \"\"){
					self.style.backgroundImage = 'none';
				}
				else{
					self.style.background = \"url('searchbar.png')\";
				}
			}
			function responsive_menu(){
				if(document.getElementById('menu_right_options_responsive_profil').style.display != 'block' || document.getElementById('menu_right_options_responsive_settings').style.display != 'block' || document.getElementById('menu_right_options_responsive_logout').style.display != 'block'){
					document.getElementById('menu_right_options_responsive_profil').style.display = 'block';
					document.getElementById('menu_right_options_responsive_settings').style.display = 'block';
					document.getElementById('menu_right_options_responsive_logout').style.display = 'block';
				}
				else{
					document.getElementById('menu_right_options_responsive_profil').style.display = 'none';
					document.getElementById('menu_right_options_responsive_settings').style.display = 'none';
					document.getElementById('menu_right_options_responsive_logout').style.display = 'none';
				}
			}
		</script>
		<title>Paint'ISEN</title>
	</head>
	<body>
		<header>
			<div id=\"menu\">
				<table id=\"menu_left_options\">
						<tr>
							<td id=\"subscriptions_button_container\" class=\"begining_color\">
								<div id =\"subscriptions_button_container2\"><button type=\"button\" onclick=\"document.getElementById('rand').classList.add('hide');document.getElementById('link').classList.add('hide');document.getElementById('subscriptions').classList.remove('hide');document.getElementById('subscriptions_button').style.backgroundColor  = 'DarkGray ';document.getElementById('subscriptions_button').style.borderRadius  = '5px';document.getElementById('link_button').style.backgroundColor  = 'transparent';document.getElementById('rand_button').style.backgroundColor  = 'transparent'; \" id=\"subscriptions_button\"><img alt=\"SocialIcon\" src=\"socialicon.png\"/></button></div>
							</td>
							<td id=\"rand_button_container\">
								<div><button type=\"button\" onclick=\"document.getElementById('subscriptions').classList.add('hide');document.getElementById('link').classList.add('hide');document.getElementById('rand').classList.remove('hide');document.getElementById('rand_button').style.backgroundColor  = 'DarkGray ';document.getElementById('rand_button').style.borderRadius  = '5px'; document.getElementById('link_button').style.backgroundColor  = 'transparent';document.getElementById('subscriptions_button').style.backgroundColor  = 'transparent'; document.getElementById('subscriptions_button_container').classList.remove('begining_color');\" id=\"rand_button\"><img alt=\"NotificationIcon\" src=\"missingicon.png\"/></button></div>
							</td>
							<th id=\"link_button_container\">
								<div><button type=\"button\" onclick=\"document.getElementById('subscriptions').classList.add('hide');document.getElementById('rand').classList.add('hide');document.getElementById('link').classList.remove('hide');document.getElementById('link_button').style.backgroundColor  = 'DarkGray ';document.getElementById('link_button').style.borderRadius  = '5px';document.getElementById('rand_button').style.backgroundColor  = 'transparent';document.getElementById('subscriptions_button').style.backgroundColor  = 'transparent'; document.getElementById('subscriptions_button_container').classList.remove('begining_color'); \" id=\"link_button\"><img alt=\"RecommandationIcon\" src=\"missingicon.png\"/></button></div>
							</th>
						</tr>
				</table>
				<div id=\"menu_center_options\">
					<table>
						<tr>
							<th>
								<div>
									<a href=\"index.php\">
										<img alt=\"Accueil\" src=\"logo.png\" id=\"menu_center_logo\" title=\"Accueil\"/>
									</a>
								</div>
							</th>
							<th>
								<div>
                                    <form method=\"POST\" action=\"#\">
                                        <input name=\"searchbar\" size=\"30\" id=\"menu_center_search\" type=\"text\" onchange=\"hideIcon(this);\" style=\"text-align:center;\"/>
                                    </form>
								</div>
							</th>
							<th>
								<div>
									<table id= \"menu_right_options_responsive\">
                                        <tr>
                                            <th>
                                                <button id=\"menu_right_options_responsive_button\" type=\"button\" onclick=\"responsive_menu()\"><img alt=\"Menu\" src=\"menu.png\"/></button>
                                            </th>
                                        </tr>
										<tr>
											<td>
												<a href=\"index.php\" id=\"menu_right_options_responsive_settings\">
													<img alt=\"Dessiner\" src=\"pencil.png\" title=\"Dessiner\"/>
												</a>
											</td>
										</tr>
										<tr>
											<td>";$rand = sha1(rand(0,9999999999999999));echo "
                                                <script style=\"display:none;\">
                                                function form_link_$rand(){document.profil_link_$rand.submit();}</script>
                                                <form name=\"profil_link_$rand\" method=\"POST\" style=\"display:inline;\" action=\"profil.php\">
                                                <input name=\"profil_link\" type=\"hidden\" value=\"";echo $user->getUsername();echo "\"/>
                                                <a href=\"javascript: form_link_$rand()\" id=\"menu_right_options_responsive_profil\">
													<img alt=\"Profil\" src=\"profil.png\" title=\"Profil\"/>
												</a>
                                                </form>
											</td>
										</tr>
										<tr>
											<th>
												<a href=\"goodbye.php\" id=\"menu_right_options_responsive_logout\">
													<img alt=\"Déconnexion\" src=\"logout.png\" title=\"Déconnexion\"/>
												</a>
											</th>
										</tr>
									</table>
								</div>
							</th>
						</tr>
					</table>
				</div>
				<video style=\"display:none;\" muted autoplay><source src= \"goodbye.mp4\"></video>
				<div id=\"menu_right_options\">
					<div id=\"normal_menu_right_options\">
                        <div>
							<a href=\"index.php\">
								<img alt=\"Dessiner\" src=\"pencil.png\" title=\"Dessiner\"/>
							</a>
						</div>
						<div>";$rand = sha1(rand(0,9999999999999999));echo "
							<script style=\"display:none;\">
                            function form_link_$rand(){document.profil_link_$rand.submit();}</script>
                            <form name=\"profil_link_$rand\" method=\"POST\" style=\"display:inline;\" action=\"profil.php\">
                            <input name=\"profil_link\" type=\"hidden\" value=\"";echo $user->getUsername();echo "\"/>
                            <a href=\"javascript: form_link_$rand()\">
                                <img alt=\"Profil\" src=\"profil.png\" title=\"Profil\"/>
                            </a>
                            </form>
						</div>
						<div>
							<a href=\"goodbye.php\">
								<img alt=\"Déconnexion\" src=\"logout.png\" title=\"Déconnexion\"/>
							</a>
						</div>
					</div>
				</div>
			</div>
		</header>
		<aside>
			<div id=\"subscriptions\">
				<h1>
					Abonnements
				</h1>
				<br/>
				<div id=\"sub_content\">";
                    $dataBase->displayFollowers($user->getUser_id());
                    
            echo "</div>
			</div>
			<div id=\"rand\" class=\"hide\">
				<h1>
					Notifications
				</h1>
				<br/>
			</div>
			<div id=\"link\" class=\"hide\">
				<h1>
					Recommandations
				</h1>
				<br/>
			</div>
		</aside>
		<div id=\"main_content\">
			<section id=\"daily_message\">
				<h1>";
                        if(date('H')>17)
                                echo  'Bonsoir ';
                        else 
                                echo  'Bonjour '; 
                echo $user->getUsername();
                echo ", comment allez-vous aujourd'hui ?
                </h1>
			</section>
			<section id=\"daily_top\">
				<h2 id=\"daily_top_title\">
					Nous vous avons sélectionné les 10 images les plus populaires du jour
				</h2>
				<article id=\"daily_top_photos\">";
					$dataBase->displayDailyTop();
            echo "</article>
			</section>
			<section id=\"posts\">
				<h2 id=\"posts_title\">
					Fil d'actualités
				</h2>
				<br/>
				<br/>";
				if(isset($_POST['searchbar'])){
                    $dataBase->displayPosts($user->getUser_id(),htmlspecialchars($_POST['searchbar']));
				}else{
                    $dataBase->displayPosts($user->getUser_id());
                }
				
            echo "Remarque:<ul style='color:red'>
				<li>1.Appel AJAX tous les 5 articles lus pour afficher les 5 suivants </li></ul>
			</section>
		</div>
		<footer>
			© 2018 Paint'ISEN - Projet WebDev réalisé par VOLTO Christophe et MARIE Kyllian
		</footer>
	</body>
</html>";
}
else{
    header('Location: index.php');
}
?>
